<?php

function calculatePrice(float $price, float $vat) : float
{
    return $price + (($price *$vat) / 100);
}

function numberFormatVnd($number)
{
    return number_format($number, 0, '', ',');
}

function qhn_uniq_id()
{
    return md5(time()) . uniqid();
}

function qhn_format_number($number)
{
    return round($number, 2);
}
