<?php

function parseAttributeToString(string $attribute) : string
{
    $attribute = str_replace('_', ' ', $attribute);
    return ucwords($attribute);
}

if(!function_exists('renderInputLivewire'))
{
    function renderInputLivewire(\Illuminate\Support\ViewErrorBag $errors, string $attribute, array $options = [])
    {

        if(empty($options['type'])) $options['type'] = 'text';
        if(empty($options['label'])) $options['label'] = parseAttributeToString($attribute);

        if(empty($options['wire_model'])) $options['wire_model'] = "wire:model.defer";

        $errorMgs = '';
        if($errors->has($attribute)){
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">'. $errors->first($attribute) .'</li>
            </ul>
        ';
        }

        return '
             <div class="form-group">
                <label>'. $options['label'] .'</label>
                <div>
                    <input
                       '. $options['wire_model'] .'="'. $attribute .'"
                        type="'. $options['type'] .'"
                        class="form-control '. ( $errors->has($attribute) ? 'parsley-error' : '' ) .'"
                    />
                </div>
                '. $errorMgs .'
            </div>
        ';
    }
}

if(!function_exists('renderSelectLivewire'))
{
    function renderSelectLivewire(\Illuminate\Support\ViewErrorBag $errors, string $attribute, $items, array $options)
    {
        if (empty($options['label'])) $options['label'] = parseAttributeToString($attribute);
        if(empty($options['selectEvent'])) $options['selectEvent'] = '';

        $htmlOptions = '';
        if (empty($options['selected'])) $options['selected'] = '';
        if (!empty(old($attribute))) $options['selected'] = old($attribute);

        if(count($items))
        {
            foreach ($items as $item) {
                if (is_array($item)) $item = (object)$item;

                if ($item->{$options['key']} == $options['selected'])
                    $htmlOptions .= '<option value="' . $item->{$options['key']} . '" selected="selected">' . $item->{$options['value']} . '</option>';
                else
                    $htmlOptions .= '<option value="' . $item->{$options['key']} . '">' . $item->{$options['value']} . '</option>';
            }
        }

        $errorMgs = '';
        if ($errors->has($attribute)) {
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">' . $errors->first($attribute) . '</li>
            </ul>
        ';
        }

        $selectClass = 'form-control ';

        if(!empty($options['selectClass'])) $selectClass .= $options['selectClass'];


        return '
            <div class="form-group">
                <label>' . $options['label'] . '</label>
                <select
                    class="'. $selectClass .'"
                    wire:model="' . $attribute . '" '. $options['selectEvent'] .'
                    name="'. $attribute .'"
                    id="'. str_replace('.', '_', $attribute) .'"
                >
                    ' . $htmlOptions . '
                </select>
                ' . $errorMgs . '
            </div>
        ';
    }
}

if(!function_exists('renderTextareaLivewire'))
{
    function renderTextareaLivewire(\Illuminate\Support\ViewErrorBag $errors, string $attribute, array $options = [])
    {
        if(empty($options['type'])) $options['type'] = 'text';
        if(empty($options['label'])) $options['label'] = parseAttributeToString($attribute);

        $errorMgs = '';
        if($errors->has($attribute)){
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">'. $errors->first($attribute) .'</li>
            </ul>
        ';
        }

        return '
             <div class="form-group">
                <label>'. $options['label'] .'</label>
                <div>
                    <textarea wire:model.defer="'. $attribute .'" class="form-control '. ( $errors->has($attribute) ? 'parsley-error' : '' ) .'"></textarea>
                </div>
                '. $errorMgs .'
            </div>
        ';
    }
}

function renderButtonStatus(string $status)
{
    $classStatus = $status === qhn_status_draft ? 'badge badge-soft-danger' : 'badge badge-info';
    return '
        <div class="'. $classStatus .'">'. __($status) .'</div>
    ';
}

function renderInputSearch()
{
    return '
        <div class="input-group">
            <input style="width: 200px" wire:model.debounce.300ms="search" type="text" class="form-control qhn-input-search" placeholder="'. __('Search ...') .'">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
        </div>
    ';
}

function renderSelectPerPage()
{
    return '
        <select wire:model="perPage" class="form-control" style="margin-right: 5px; width: auto">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
    ';
}

function renderSelectFilterStatus()
{
    return '
        <select wire:model="status" class="form-control" style="margin-right: 5px; width: auto">
            <option value="">'. __('All') .'</option>
            <option value="'. qhn_status_completed .'">'. __('Completed') .'</option>
            <option value="'. qhn_status_draft .'">'. __('Draft') .'</option>
        </select>
    ';
}

//function dumpSql(Builder $builder) : string
//{
//    $sql_with_bindings = \Str::replaceArray('?', $builder->getBindings(), $builder->toSql());
//    dd($sql_with_bindings);
//}
