<?php

const qhn_class_btn_edit = 'btn btn-outline-info btn-sm';

const qhn_icon_edit = 'far fa-edit';

const qhn_class_btn_delete = 'btn btn-outline-danger btn-sm';

const qhn_icon_delete = 'far fa-trash-alt';

const qhn_icon_show = 'fas fa-eye';

const qhn_icon_create = 'fas fa-plus';

const qhn_class_btn_create = 'btn btn-default btn-primary qhn-btn-create-data';

const qhn_status_draft = 'draft';

const qhn_status_completed = 'completed';

const qhn_role_admin = 100;

const qhn_role_client = 20;
