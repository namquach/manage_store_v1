<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('language/switch/{language}', 'LocaleController@switch')->name('language.switch');

Route::get('login', 'AuthController@login')->name('auth.login');
Route::get('logout', 'AuthController@logout')->name('auth.logout');
Route::get('forgot-password', 'AuthController@forgotPassword')->name('auth.forgot-password');
Route::get('update-forgot-password', 'AuthController@updateForgotPassword')->name('auth.update-forgot-password');

Route::middleware(['auth.admin.login'])->prefix('admin')->group(function () {
    Route::prefix('log-error')->group(function (){
        Route::get('/', 'AdminLogErrorController@index')->name('admin.log-error.index');
        Route::get('show/{id}', 'AdminLogErrorController@show')->name('admin.log-error.show');
    });
});

Route::middleware(['auth.login'])->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    Route::prefix('product-group')->group(function () {
        Route::get('/', 'ProductGroupController@index')->name('product-group.index');
        Route::get('create', 'ProductGroupController@create')->name('product-group.create');
        Route::get('update/{id}', 'ProductGroupController@update')->name('product-group.update');
    });

    Route::prefix('product')->group(function () {
        Route::get('/', 'ProductController@index')->name('product.index');
        Route::get('create', 'ProductController@create')->name('product.create');
        Route::get('update/{id}', 'ProductController@update')->name('product.update');
        Route::get('search', 'ProductController@search')->name('product.search');
    });

    Route::prefix('customer')->group(function () {
        Route::get('/', 'CustomerController@index')->name('customer.index');
        Route::get('create', 'CustomerController@create')->name('customer.create');
        Route::get('update/{id}', 'CustomerController@update')->name('customer.update');
        Route::get('search', 'CustomerController@search')->name('customer.search');
    });

    Route::prefix('supplier')->group(function () {
        Route::get('/', 'SupplierController@index')->name('supplier.index');
        Route::get('create', 'SupplierController@create')->name('supplier.create');
        Route::get('update/{id}', 'SupplierController@update')->name('supplier.update');
    });

    Route::prefix('purchase')->group(function () {
        Route::get('/', 'PurchaseController@index')->name('purchase.index');
        Route::get('create', 'PurchaseController@create')->name('purchase.create');
        Route::get('update/{id}', 'PurchaseController@update')->name('purchase.update');
    });

    Route::prefix('order')->group(function () {
        Route::get('/', 'OrderController@index')->name('order.index');
        Route::get('create', 'OrderController@create')->name('order.create');
        Route::get('update/{id}', 'OrderController@update')->name('order.update');
    });

    Route::prefix('setting-price')->group(function () {
        Route::get('/', 'SettingPriceController@index')->name('setting-price.index');
        Route::get('create', 'SettingPriceController@create')->name('setting-price.create');
        Route::get('update/{id}', 'SettingPriceController@update')->name('setting-price.update');
    });
});
