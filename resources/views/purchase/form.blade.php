@extends('layout.master')

@section('breadcrumb')
    <h4 class="page-title mb-1">{{ __('Purchase') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('product-group.index') }}">{{ __('Purchase') }}</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('purchase.purchase-form', ['purchase' => $purchase, 'isCreate' => $isCreate])
                </div>
            </div>
        </div>
    </div>
@endsection
