@extends('auth.master')

@section('title')
    {{ __('Login') }}
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">
                        <h5 class="mb-5 text-center" style="margin-bottom: 0px">Sign in to continue to Xoric.</h5>
                        @livewire('auth.login-form')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
