
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') | QHN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesdesign" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('theme/assets/images/favicon.ico') }}">
    <link href="{{ asset('theme/assets/libs/spectrum-colorpicker/spectrum.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/assets/libs/selectize/css/selectize.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('theme/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />

    <!-- Bootstrap Css -->
    <link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('theme/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('theme/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    @livewireStyles
</head>

<body class="bg-primary bg-pattern">

<div class="account-pages my-5 pt-sm-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center mb-5">
                    <a href="index.html" class="logo"><img src="{{ asset('theme/assets/images/logo-light.png') }}" height="24" alt="logo"></a>
                    <h5 class="font-size-16 text-white-50 mb-4">Responsive Bootstrap 4 Admin Dashboard</h5>
                </div>
            </div>
        </div>
        <!-- end row -->

        @yield('content')
        <!-- end row -->
    </div>
</div>
<!-- end Account pages -->

<!-- JAVASCRIPT -->
<script src="{{ asset('theme/assets/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/node-waves/waves.min.js') }}"></script>

<script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>

<script src="{{ asset('theme/assets/libs/spectrum-colorpicker/spectrum.js') }}"></script>
<script src="{{ asset('theme/assets/libs/selectize/js/standalone/selectize.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<script src="{{ asset('theme/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script src="{{ asset('theme/assets/js/pages/form-advanced.init.js') }}"></script>
<script src="{{ asset('theme/assets/js/app.js') }}"></script>
<script>
    $("input#password").maxlength({
        threshold: 20,
        warningClass: "badge badge-info",
        limitReachedClass: "badge badge-warning"
    })
</script>
@livewireScripts
</body>
</html>
