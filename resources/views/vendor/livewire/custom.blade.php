@if ($paginator->hasPages())
    <ul class="flex justify-between pagination float-right">
        <!-- prev -->
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a href="#" class="page-link" wire:click.prevent>Prev</a>
            </li>
        @else
            <li class="page-item" wire:click="previousPage">
                <a href="#" class="page-link" wire:click.prevent>Prev</a>
            </li>
        @endif
    <!-- prev end -->
        <!-- numbers -->
        @foreach ($elements as $element)
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active" wire:click="gotoPage({{$page}})">
                            <a href="#" class="page-link" wire:click.prevent>{{$page}}</a>
                        </li>
                    @else
                        <li class="page-item" wire:click="gotoPage({{$page}})">
                            <a href="#" class="page-link" wire:click.prevent>{{$page}}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach
    <!-- end numbers -->


        <!-- next  -->
        @if ($paginator->hasMorePages())
            <li class="page-item" wire:click="nextPage"><a class="page-link" wire:click.prevent>Next</a></li>
        @else
            <li class="page-item disabled">
                <a href="#" class="page-link" wire:click.prevent>Next</a>
            </li>
    @endif
    <!-- next end -->
    </ul>
@endif
