<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Starter page | Xoric - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="{{ asset('theme/assets/images/favicon.ico') }}">
    <link href="{{ asset('theme/assets/libs/RWD-Table-Patterns/css/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/assets/libs/alertifyjs/build/css/alertify.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('theme/assets/libs/selectize/css/selectize.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('theme/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    @livewireStyles

</head>

<body data-topbar="colored">

<!-- Begin page -->
<div id="layout-wrapper">

@include('admin-layout.header')
<!-- ========== Left Sidebar Start ========== -->
@include('admin-layout.sidebar')
<!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">

            <!-- Page-Title -->
            <div class="page-title-box">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            @yield('admin.breadcrumb')
                        </div>
                    </div>

                </div>
            </div>
            <!-- end page title end breadcrumb -->

            <div class="page-content-wrapper">
                <div class="container-fluid">
                @yield('admin.content')
                <!-- end row -->

                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end page-content-wrapper -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        2020 © Xoric.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesdesign
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- JAVASCRIPT -->
<script src="{{ asset('theme/assets/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script src="{{ asset('theme/assets/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/node-waves/waves.min.js') }}"></script>

{{--<script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>--}}

<script src="{{ asset('theme/assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/alertifyjs/build/alertify.min.js') }}"></script>
<script src="{{ asset('theme/assets/js/app.js') }}"></script>
@livewireScripts
<script>
    window.addEventListener('swal.confirm', event => {
        Swal.fire({
            showCancelButton: true,
            confirmButtonColor: "#3ddc97",
            cancelButtonColor: "#f46a6a",
            title: event.detail.title,
            text: event.detail.text,
            icon: event.detail.icon,
        }).then(willDelete => {
            if(willDelete.value === true) {
                Livewire.emit('delete', event.detail.id);
            }
        })
    })

    window.addEventListener('alertify.confirm.success', event => {
        alertify.success(event.detail.message);
    })

    window.addEventListener('alertify.confirm.error', event => {
        alertify.error(event.detail.message);
    })

    window.addEventListener('alertify.confirm.warning', event => {
        alertify.error(event.detail.message);
    })

    window.addEventListener('event.closeModal', event => {
        Livewire.emit('hideModal');
        $('.modal').modal('hide');
    })

    $('.select2').select2();
</script>
@yield('admin.script')
@stack('admin.scripts')

</body>
</html>
