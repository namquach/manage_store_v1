<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('supplier.create') }}" class="{{ qhn_class_btn_create }}">
                        <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    <select wire:model="perPage" class="form-control" style="margin-right: 5px; width: auto">
                        @foreach($filterPerPages as $key => $filterPerPage)
                            <option value="{{ $key }}">{{ $filterPerPage }}</option>
                        @endforeach
                    </select>
                    <div class="input-group">
                        <input style="width: 200px" wire:model.debounce.300ms="search" type="text" class="form-control" placeholder="{{ __('Search ...') }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if($suppliers->count())
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Province') }}</th>
                            <th>{{ __('District') }}</th>
                            <th>{{ __('Ward') }}</th>
                            <th>{{ __('Address') }}</th>
                            <th>{{ __('Updated') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($suppliers as $supplier)
                            <tr>
                                <td style="width: 4%; text-align: center">{{ $supplier->id }}</td>
                                <td>{{ $supplier->name }}</td>
                                <td>{{ $supplier->province }}</td>
                                <td>{{ $supplier->district }}</td>
                                <td>{{ $supplier->ward }}</td>
                                <td>{{ $supplier->address }}</td>
                                <td>{{ $supplier->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('supplier.update', ['id' => $supplier->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $supplier->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $suppliers->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        {{ __('Can not found suppliers') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

