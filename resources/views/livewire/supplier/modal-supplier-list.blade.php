<div wire:ignore.self data-backdrop="static"  id="modalSupplier" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('Suppliers') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="$emit('hideModal')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div wire:loading>
                        <x-loading />
                    </div>
                    <div class="table-rep-plugin">
                        <div class="table-wrapper">
                            <div class="btn-toolbar" style="margin-bottom: 30px">
                                <div class="btn-group focus-btn-group">
                                </div>
                                <div class="btn-group dropdown-btn-group pull-right">
                                    <select wire:model="perPage" class="form-control" style="margin-right: 5px; width: auto">
                                        @foreach($filterPerPages as $key => $filterPerPage)
                                            <option value="{{ $key }}">{{ $filterPerPage }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group">
                                        <input style="width: 200px" wire:model.debounce.300ms="search" type="text" class="form-control" placeholder="{{ __('Search ...') }}">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($suppliers->count() > 0)
                                <div class="table-responsive mb-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Province') }}</th>
                                        <th>{{ __('District') }}</th>
                                        <th>{{ __('Ward') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($suppliers as $supplier)
                                        <tr>
                                            <td style="width: 4%; text-align: center">{{ $supplier->id }}</td>
                                            <td>{{ $supplier->name }}</td>
                                            <td>{{ $supplier->province }}</td>
                                            <td>{{ $supplier->district }}</td>
                                            <td>{{ $supplier->ward }}</td>
                                            <td>{{ $supplier->address }}</td>
                                            <td></td>
                                            <td>
                                                <button class="btn btn-success btn-sm" wire:click="$emit('eventChooseSupplier', {{ $supplier }})">{{ __('Choose') }}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $suppliers->links() !!}
                            </div>
                            @else
                                <div class="alert alert-primary" role="alert" style="text-align: center">
                                    {{ __('Can not found suppliers') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelSupplierChange', function () {
            console.log('call')
            if($('#modalSupplier').length > 0){
                $('#modalSupplier').modal('show');
            }
        })
    </script>
@endpush
