<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                <h5 class="card-header bg-transparent border-bottom mt-0">
                    <a href="{{ route('setting-price.index') }}" class="btn btn-secondary" type="button">{{ __('Back') }}</a>
                    <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
                </h5>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! renderInputLivewire($errors, 'settingPrice.name', ['label' => __('Name')]) !!}
            </div>
        </div>
        <div>
            @livewire('setting-price.setting-price-customer-form', ['settingPrice' => $settingPrice])
        </div>
        <div>
            @livewire('setting-price.setting-price-product-table', ['settingPrice' => $settingPrice])
        </div>
    </form>
</div>
