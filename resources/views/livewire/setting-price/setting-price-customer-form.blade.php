<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="qhn-cus-box">
                <div wire:ignore wire:key="customers" class="form-group" style="margin-top: 5px">
                    <label>{{ __('Customers') }}</label>
                    <select wire:model="customers" style="width: 100%" class="form-control select2" name="customers" id="customers">
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="qhn-cus-box">
                @if (count($customers) > 0)
                    <div class="mt-4">
                        @foreach($customers as $customer)
                            @php
                                $infoTooltip = __('Name: ') . $customer->customer->name . '<br>';
                                $infoTooltip .= __('Address: ') . join(' ', [
                                    $customer->customer->address,
                                    $customer->customer->ward,
                                    $customer->customer->district,
                                    $customer->customer->province,
                                ])
                            @endphp
                            <a href="#" class="btn btn-primary btn-sm" style="margin-bottom: 5px; margin-right: 5px" data-html="true" data-toggle="tooltip" data-original-title="{!! $infoTooltip !!}">
                                {{ $customer->customer->name }}
                                <i class="fas fa-window-close"  style="color: tomato" wire:click="deleteSettingPriceCustomer({{ $customer }})"></i>
                            </a>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        window.addEventListener('event.autoFocusSelect2Customer', event => {
            $('#customers').select2('open');
        });

        $('#customers').change(function (event) {
            //@this.set('customers', event.target.value);
            Livewire.emit('eventAddSettingPriceCustomer', event.target.value);
        })

        $('#customers').select2({
            placeholder: "{{ __('Choose customer') }}",
            ajax: {
                url: '{{ route('customer.search') }}',
                dataType: 'json',
                data: function (params) {
                    let query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: JSON.stringify(item),
                                data: item
                            }
                        })
                    };
                }
            }
        });
    </script>
@endpush
