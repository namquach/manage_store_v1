<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('setting-price.create', ['tmp' => qhn_uniq_id()]) }}" class="{{ qhn_class_btn_create }}">
                        <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if($settingPrices->count())
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Updated') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($settingPrices as $settingPrice)
                            <tr>
                                <td style="width: 4%; text-align: center">{{ $settingPrice->id }}</td>
                                <td>{{ $settingPrice->name }}</td>
                                <td>{{ $settingPrice->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('setting-price.update', ['id' => $settingPrice->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $settingPrice->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $settingPrices->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        {{ __('Can not found setting price') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

