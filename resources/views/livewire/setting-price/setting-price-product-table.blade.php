<div class="card qhn-cus-box" style="margin-top: 10px;">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div wire:loading>
                    <x-loading />
                </div>
                @if($stateModalImport)
                    @livewire('setting-price.modal-import-setting-price-product', ['settingPrice' => $settingPrice])
                @endif
                <div class="table-rep-plugin">
                    <div class="table-wrapper">
                        <div class="btn-toolbar" style="margin-bottom: 30px;">
                            <div class="btn-group focus-btn-group">
                                <button type="button" wire:click="updateStateImport" class="btn btn-primary">{{ __('Import') }}</button>
                            </div>
                            <div class="btn-group dropdown-btn-group pull-right">
                                {!! renderSelectPerPage() !!}
                                {!! renderInputSearch() !!}
                            </div>
                        </div>
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            @if($settingPriceProducts->count())
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">#</th>
                                        <th>{{ __('Sku') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Price') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($settingPriceProducts as $settingPriceProduct)
                                        <tr>
                                            <td style="width: 4%; text-align: center">{{ $settingPriceProduct->id }}</td>
                                            <td>{{ $settingPriceProduct->product->sku }}</td>
                                            <td>{{ $settingPriceProduct->product->name }}</td>
                                            <td>
                                                <input
                                                    type="number"
                                                    value="{{ qhn_format_number($settingPriceProduct->price) }}"
                                                    class="form-control setting-price-product"
                                                    name="setting-price-product[]"
                                                    id="setting-price-product-{{ $settingPriceProduct->id }}"
                                                    wire:change="changeSettingPriceProduct({{$settingPriceProduct}}, event.target.value)"
                                                />
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $settingPriceProducts->links() !!}
                            @else
                                <div class="alert alert-primary" role="alert" style="text-align: center">
                                    {{ __('Can not found products') }}
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateImportSettingPriceProduct', function () {
            if($('#modalImportSettingPriceProduct').length > 0){
                $('#modalImportSettingPriceProduct').modal('show');
            }
        })
    </script>
@endpush

