<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('purchase.create', ['tmp' => qhn_uniq_id()]) }}" class="{{ qhn_class_btn_create }}">
                        <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectFilterStatus() !!}
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if($purchases->count() > 0)
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Customer Name') }}</th>
                            <th>{{ __('Customer Address') }}</th>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Updated') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($purchases as $purchase)
                            <tr>
                                <td style="width: 4%; text-align: center">{{ $purchase->id }}</td>
                                <td>{{ $purchase->supplier_name }}</td>
                                <td>{{ $purchase->supplier_address }}</td>
                                <td>{{ $purchase->date }}</td>
                                <td>
                                    {!! renderButtonStatus($purchase->status) !!}
                                </td>
                                </td>
                                <td>{{ $purchase->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('purchase.update', ['id' => $purchase->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $purchase->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $purchases->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        {{ __('Can not found purchases') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

