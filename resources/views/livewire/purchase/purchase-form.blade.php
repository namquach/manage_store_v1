<div>
    <div wire:loading>
        <x-loading />
    </div>
    @if($stateModalSupplier)
        @livewire('supplier.modal-supplier-list')
    @endif
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                <h5 class="card-header bg-transparent border-bottom mt-0">
                    <a href="{{ route('purchase.index') }}" class="btn btn-secondary" type="button">{{ __('Back') }}</a>
                    @if($purchase->status === qhn_status_draft)
                        <button class="btn btn-info">{{ __('Save') }}</button>
                        @if (!$isCreate)
                            <button wire:click="updateStatusPurchase" class="btn btn-success" type="button">{{ __('Completed') }}</button>
                        @endif
                    @endif
                </h5>
                <br />
            </div>
        </div>

        <input type="hidden" wire:model="purchase.supplier_id" />

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <label for="">{{ __('Customer Name') }}</label>
                    <div class="input-group mb-2">
                        <input wire:model="purchase.supplier_name" type="text" class="form-control" placeholder="{{ __('Choose supplier') }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text btn btn-primary" wire:click="eventOpenModalSupplier">
                                <i class="far fa-user"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 15px">
                    {!! renderTextareaLivewire($errors, 'purchase.supplier_address', ['label' => __('Supplier Address')]) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    {!! renderInputLivewire($errors, 'purchase.date', ['label' => __('Date'), 'type' => 'date']) !!}
                </div>
                <div class="col-md-12">
                    {!! renderTextareaLivewire($errors, 'purchase.note', ['label' => __('Note')]) !!}
                </div>
            </div>
        </div>
    </form>

    <div>
        @livewire('purchase-line.purchase-line-table', ['purchase' => $purchase])
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelSupplierChange', function () {
            if($('#modalSupplier').length > 0){
                $('#modalSupplier').modal('show');
            }
        })
    </script>
@endpush
