<div wire:ignore.self data-backdrop="static"  id="modalImportProduct" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('Import product') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="$emit('hideModal')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div wire:loading wire:target="submit">
                    <x-loading />
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-file">
                                <input wire:model.defer="file" type="file" class="custom-file-input" id="file">
                                <label class="custom-file-label" for="file">{{ $label }}</label>
                            </div>

                            @if(session()->has('import_product_error'))
                                <span class="error">{{ session()->get('import_product_error') }}</span>
                            @endif
                        </div>
                        @if(session()->has('import_product_success'))
                            <div class="col-md-12">
                                <div class="alert alert-success" style="margin-top: 10px">
                                    <p>{{ session()->get('import_product_success') }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-12 m-3 ">
                            <button class="btn btn-outline-secondary" type="submit" wire:click="submit">{{ __('Import') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
