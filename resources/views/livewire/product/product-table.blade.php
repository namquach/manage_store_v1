<div>
    <div wire:loading>
        <x-loading />
    </div>

    @if ($stateModalImportProduct)
        @livewire('product.modal-import')
    @endif

    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('product.create') }}" class="{{ qhn_class_btn_create }}">
                        <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                    <a style="margin-left: 10px" class="btn btn-success btn-default" wire:click="showModalImportProduct">
                        {{ __('Import') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if ($products->count())
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Sku') }}</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Price') }}</th>
                            <th>{{ __('Vat %') }}</th>
                            <th>{{ __('Stock') }}</th>
                            <th>{{ __('Store In') }}</th>
                            <th>{{ __('Store Out') }}</th>
                            <th>{{ __('Updated') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td style="width: 4%; text-align: center">{{ $product->id }}</td>
                                <td>{{ $product->sku }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->vat }}</td>
                                <td>{{ $product->stock }}</td>
                                <td>{{ $product->store_in }}</td>
                                <td>{{ $product->store_out }}</td>
                                <td>{{ $product->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('product.update', ['id' => $product->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $product->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $products->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        No Product Line
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModalImportProduct', function () {
            if($('#modalImportProduct').length > 0){
                $('#modalImportProduct').modal('show');
            }
        })
    </script>
@endpush
