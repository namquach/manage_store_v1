<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                <h5 class="card-header bg-transparent border-bottom mt-0">
                    <a href="{{ route('product.index') }}" class="btn btn-secondary" type="button">{{ __('Back') }}</a>
                    <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
                </h5>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! renderInputLivewire($errors, 'product.sku', ['label' => __('Sku')]) !!}
                {!! renderInputLivewire($errors, 'product.name', ['label' => __('Name')]) !!}
            </div>
            <div class="col-md-3">
                {!! renderInputLivewire($errors, 'product.price', ['label' => __('Price'), 'type' => 'number']) !!}
            </div>

            <div class="col-md-3">
                {!! renderInputLivewire($errors, 'product.vat', ['label' => __('Vat %'), 'type' => 'number']) !!}
            </div>
            <div class="col-md-3"  wire:ignore>
                {!! renderSelectLivewire($errors, 'product.unit', $productUnit, [
                        'label' => __('Unit'),
                        'selectClass' => 'select2',
                        'key' => 'id',
                        'value' => 'name',
                        'selected' => $product->unit
                    ]) !!}
            </div>
            <div class="col-md-3">
                {!! renderInputLivewire($errors, 'product.quantity_container_by_can', ['label' => __('Quantity Container By Can'), 'type' => 'number']) !!}
            </div>
            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'product.group1', $productGroups->where('group', 1),[
                            'key' => 'id',
                            'value' => 'name',
                            'label' => __('Group 1'),
                            'selectEvent' => 'wire:change="onChangeGroup($event.target.value, 1)"',
                        ]) !!}
            </div>

            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'product.group2', $productGroups->where('group', 2),[
                            'key' => 'id',
                            'value' => 'name',
                            'label' => __('Group 2'),
                            'selectEvent' => 'wire:change="onChangeGroup($event.target.value, 2)"',
                        ]) !!}
            </div>

            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'product.group3', $productGroups->where('group', 3),[
                            'key' => 'id',
                            'value' => 'name',
                            'label' => __('Group 3'),
                            'selectEvent' => 'wire:change="onChangeGroup($event.target.value, 3)"',
                        ]) !!}
            </div>
            <div class="col-md-12">
                {!! renderTextareaLivewire($errors, 'product.note', ['label' => __('Note')]) !!}
            </div>
        </div>
        {{--        <button class="btn btn-info" type="submit">{{ trans('messages.Save and new') }}</button>--}}
    </form>
</div>

@push('scripts')
    <script>
        $('#product_unit').on('change', function (event) {
            @this.set('product.unit', event.target.value);
        })
    </script>
@endpush
