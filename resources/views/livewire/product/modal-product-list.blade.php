<div wire:ignore.self  id="modalProduct" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('Products') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div wire:loading>
                        <x-loading />
                    </div>
                    <div class="table-rep-plugin">
                        <div class="table-wrapper">
                            <div class="btn-toolbar">
                                <div class="btn-group focus-btn-group">
                                </div>
                                <div class="btn-group dropdown-btn-group pull-right">
                                    {!! renderSelectPerPage() !!}
                                    {!! renderInputSearch() !!}
                                </div>
                            </div>
                            <div class="table-responsive mb-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">#</th>
                                        <th>{{ __('Sku') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Price') }}</th>
                                        <th>{{ __('Stock') }}</th>
                                        <th>{{ __('Quantity') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td style="width: 4%; text-align: center">{{ $product->id }}</td>
                                            <td>{{ $product->sku }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>
                                                @if($isOrder)
                                                    {{ numberFormatVnd(calculatePrice($product->price, $product->vat)) }}
                                                @else
                                                    {{ numberFormatVnd($product->price) }}
                                                @endif
                                            </td>
                                            <td>{{ $product->stock }}</td>
                                            <td>
                                                <input wire:click="" wire:model="quantity.{{ $product->id }}" type="number" class="form-control" style="width: 100px" />
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-sm" wire:click="eventOnChooseProduct({{ $product }})">{{ __('Choose') }}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $products->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
