<div wire:ignore.self data-backdrop="static"  id="modalCustomer" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('Customers') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="$emit('hideModal')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div wire:loading>
                        <x-loading />
                    </div>
                    <div class="table-rep-plugin">
                        <div class="table-wrapper">
                            <div class="btn-toolbar" style="margin-bottom: 30px;">
                                <div class="btn-group focus-btn-group">
                                </div>
                                <div class="btn-group dropdown-btn-group pull-right">
                                    {!! renderSelectPerPage() !!}
                                    {!! renderInputSearch() !!}
                                </div>
                            </div>
                            @if($customers->count() > 0)
                                <div class="table-responsive mb-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Province') }}</th>
                                        <th>{{ __('District') }}</th>
                                        <th>{{ __('Ward') }}</th>
                                        <th>{{ __('Address') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($customers as $customer)
                                        <tr>
                                            <td style="width: 4%; text-align: center">{{ $customer->id }}</td>
                                            <td>{{ $customer->name }}</td>
                                            <td>{{ $customer->province }}</td>
                                            <td>{{ $customer->district }}</td>
                                            <td>{{ $customer->ward }}</td>
                                            <td>{{ $customer->address }}</td>
                                            <td></td>
                                            <td>
                                                <button class="btn btn-success btn-sm" wire:click="$emit('eventChooseCustomer', {{ $customer }})">{{ __('Choose') }}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $customers->links() !!}
                            </div>
                            @else
                                <div class="alert alert-primary" role="alert" style="text-align: center">
                                    {{ __('Can not found customers') }}
                                </div>
                            @endif

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelCustomerChange', function () {
            console.log('call')
            if($('#modalCustomer').length > 0){
                $('#modalCustomer').modal('show');
            }
        })
    </script>
@endpush
