<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                {!! renderInputLivewire($errors, 'customer.name', ['label' => __('Name'), 'value' => $customer->customer_name]) !!}
            </div>
            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'customer.province', $provinces, [
                    'key' => 'name',
                    'value' => 'name',
                    'label' => __('Province'),
                    'selectEvent' => 'wire:change="onChangeProvince($event.target.value)"',
                ]) !!}
            </div>

            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'customer.district', $districts, [
                    'key' => 'name',
                    'value' => 'name',
                    'label' => __('District'),
                    'selectEvent' => 'wire:change="onChangeDistrict($event.target.value)"',
                ]) !!}
            </div>

            <div class="col-md-4">
                {!! renderSelectLivewire($errors, 'customer.ward', $wards, [
                    'key' => 'name',
                    'value' => 'name',
                    'label' => __('Ward'),
                ]) !!}
            </div>
            <div class="col-md-12">
                {!! renderInputLivewire($errors, 'customer.address', ['label' => __('Address')]) !!}
            </div>

            <div class="col-md-12">
                {!! renderTextareaLivewire($errors, 'customer.note', ['label' => __('Note')]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
            </div>
        </div>
        {{--        <button class="btn btn-info" type="submit">{{ trans('messages.Save and new') }}</button>--}}
    </form>
</div>
