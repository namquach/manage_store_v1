<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12 mb-3">
                {!! renderInputLivewire($errors, 'productGroup.name', ['label' => 'Name']) !!}
            </div>

            <div class="col-md-12 mb-3">
                {!! renderSelectLivewire($errors, 'productGroup.group', $group,[
                        'key' => 'key',
                        'value' => 'value',
                        'label' => __('Group')
                    ]) !!}
            </div>
        </div>
        <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
{{--        <button class="btn btn-info" type="submit">{{ trans('messages.Save and new') }}</button>--}}
    </form>
</div>
