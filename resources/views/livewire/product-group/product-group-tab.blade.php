<div class="row">
    <div class="col-sm-3">
        <div class="nav flex-column nav-pills qhn-cus-box" style="padding-top: 10px" role="tablist" aria-orientation="vertical">
            @foreach($tabList as $key => $tabList)
                <a wire:click="eventOnNextTab({{ $key }})" class="nav-link mb-2 {{ $key == $tabIndex ? 'active' : '' }}" data-toggle="pill" href="#{{ $tabList }}" role="tab" aria-selected="false">
                    <i class="fas fa-layer-group mr-1"></i> {{ $tabList }}
                </a>
            @endforeach
        </div>
    </div>
    <div class="col-sm-9">
        <div class="tab-content mt-4 mt-sm-0 qhn-cus-box" style="padding-top: 10px; padding-bottom: 10px">
            <div class="tab-pane fade active show" role="tabpanel">
                @livewire('product-group.product-group-table', ['group' => $tabIndex])
            </div>
        </div>
    </div>
</div>
