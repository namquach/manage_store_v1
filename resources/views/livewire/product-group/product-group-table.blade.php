<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('product-group.create') }}" class="{{ qhn_class_btn_create }}">
                       <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if ($productGroups->count())
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ trans('Name') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($productGroups as $productGroup)
                            <tr>
                                <td style="width: 10%; text-align: center">{{ $productGroup->id }}</td>
                                <td style="width: 75%;">{{ $productGroup->name }}</td>
                                <td style="width: 15%">
                                    <a href="{{ route('product-group.update', ['id' => $productGroup->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $productGroup->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $productGroups->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        No Product Group Line
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

