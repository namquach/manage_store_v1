<div class="card bg-primary text-white">
    <div class="card-body">
        <div class="row">
            <div wire:loading>
                <x-loading />
            </div>
            <div class="qhn-col col-md-2">
                <div wire:ignore wire:key="sku" class="form-group">
                    <label>{{ __('Sku') }}</label>
                    <select wire:model="sku" class="form-control selectize select2" id="qhn_sku">
                    </select>
                </div>
            </div>
            <input type="hidden" wire:model="product_id">
            <div class="col-md-4">
                {!! renderInputLivewire($errors, 'name', ['label' => trans('Name')]) !!}
            </div>
            <div class="col-md-1">
                {!! renderInputLivewire($errors, 'price', ['type' => 'number', 'label' => trans('Price')]) !!}
            </div>
            <div class="col-md-1">
                {!! renderInputLivewire($errors, 'quantity', ['type' => 'number', 'label' => __('Quantity'), 'wire_model' => 'wire:model.debounce.500ms']) !!}
            </div>

            <div class="col-md-1">
                {!! renderSelectLivewire($errors, 'unit', $productUnit, ['key' => 'id', 'value' => 'name', 'label' => __('Unit')]) !!}
            </div>

            <div class="col-md-2">
                {!! renderTextareaLivewire($errors, 'note', ['label' => __('Note')]) !!}
            </div>

            <div class="col-md-1 qhn-center-box" style="text-align: center">
                <button wire:click="create" class="btn btn-success">{{  __('Add') }}</button>
            </div>

            @if (!empty($more_info))
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    <th>{{ __('Unit by can') }}</th>
                                    <th>{{ __('Exchange by can') }}</th>
                                    <th>{{ __('Total quantity') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $more_info['unit_by_can'] }}</td>
                                    <td>{{ $more_info['exchange_by_can'] }}</td>
                                    <td><strong>{{ $more_info['quantity'] }}</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

</div>


@push('scripts')
    <script>
        window.addEventListener('event.autofocusSelect2Sku', event => {
            $('#qhn_sku').val(null).trigger('change');
            $('#qhn_sku').select2('open');
        })

        $(document).ready(function () {
            $('#qhn_sku').focus();
        })

        $('#qhn_sku').change(function (event) {
            //@this.set('sku', event.target.value);
            Livewire.emit('onChangeSku', event.target.value)
        })

        $('#qhn_sku').select2({
            placeholder: "{{ __('Choose sku') }}",
            ajax: {
                url: '{{ route('product.search') }}',
                dataType: 'json',
                data: function (params) {
                    let query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.sku,
                                id: item.sku
                            }
                        })
                    };
                }
            }
        });
    </script>
@endpush
