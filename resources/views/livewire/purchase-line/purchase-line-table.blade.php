<div class="card qhn-cus-box">
    <div>
        <div wire:loading>
            <x-loading />
        </div>
        <div class="card-body">
            @if($purchase->status === qhn_status_draft)
                @livewire('purchase-line.purchase-line-form', ['purchase' => $purchase, 'purchaseLines' => $purchaseLines])
            @endif
             <div class="table-rep-plugin">
                <div class="table-wrapper">
                    <div class="btn-toolbar" style="margin-bottom: 30px">
                        <div class="btn-group focus-btn-group">

                        </div>
                        <div class="btn-group dropdown-btn-group pull-right">
                            {!! renderSelectPerPage() !!}
                            {!! renderInputSearch() !!}
                        </div>
                    </div>
                    @if($purchaseLines->count() > 0)
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <table id="tech-companies-1" class="table table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th>{{ __('Sku') }}</th>
                                <th>{{ __('Product Name') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Quantity by can') }}</th>
                                <th>{{ __('Price') }}</th>
                                <th>{{ __('Total') }}</th>
                                <th>{{ __('Note') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchaseLines as $purchaseLine)
                                <tr>
                                    <td style="width: 4%; text-align: center">{{ $purchaseLine->id }}</td>
                                    <td>{{ $purchaseLine->product->sku }}</td>
                                    <td>{{ $purchaseLine->product->name }}</td>
                                    <td>{{ $purchaseLine->quantity }}</td>
                                    <td>{{ qhn_format_number($purchaseLine->quantity / $purchaseLine->unit_by_can) }}</td>
                                    <td>{{ numberFormatVnd($purchaseLine->price) }}</td>
                                    <td>
                                        <strong>{{ numberFormatVnd($purchaseLine->quantity * $purchaseLine->price) }}</strong>
                                    </td>
                                    <td>{{ $purchaseLine->note }}</td>
                                    <td>
                                        @if($purchase->status === qhn_status_draft)
                                            <button class="btn btn-danger btn-sm" wire:click="deleteConfirm({{ $purchaseLine->id }})">
                                                <i class="{{ qhn_icon_delete }}"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="6" align="center">
                                    <strong>
                                        {{ __('Total') }}
                                    </strong>
                                </td>
                                <td colspan="2">
                                    <strong>
                                        {{ numberFormatVnd($total) }}
                                    </strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {{ $purchaseLines->links() }}

                    </div>
                    @else
                        <div class="alert alert-primary" role="alert" style="text-align: center; margin-top: 8px">
                            {{ __('Can not found products') }}
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>

</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelProductChange', function () {
            if($('#modalProduct').length > 0){
                $('#modalProduct').modal('show');
            }
        })
    </script>
@endpush


