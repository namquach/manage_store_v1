<div class="card qhn-cus-box">
    <div wire:loading>
        <x-loading />
    </div>
    <div class="card-body">
        @if ($order->status === qhn_status_draft)
            @livewire('order-line.order-line-form', ['order' => $order])
        @endif
        <div class="table-rep-plugin">
            <div class="table-wrapper">
                <div class="btn-toolbar" style="margin-bottom: 30px">
                    <div class="btn-group focus-btn-group"></div>
                    <div class="btn-group dropdown-btn-group pull-right">
                        {!! renderSelectPerPage() !!}
                        {!! renderInputSearch() !!}
                    </div>
                </div>
                <div class="table-responsive mb-0" data-pattern="priority-columns">
                    @if ($orderLines->count())
                        <table id="tech-companies-1" class="table table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th>{{ __('Sku') }}</th>
                                <th>{{ __('Product Name') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Quantity by can') }}</th>
                                <th>{{ __('Price') }}</th>
                                <th>{{ __('Total') }}</th>
                                <th>{{ __('Note') }}</th>
                                <th>{{ __('Note2') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orderLines as $orderLine)
                                <tr>
                                    <td style="width: 4%; text-align: center">{{ $orderLine->id }}</td>
                                    <td>{{ $orderLine->product->sku }}</td>
                                    <td>{{ $orderLine->product->name }}</td>
                                    <td>{{ $orderLine->quantity }}</td>
                                    <td>{{ qhn_format_number($orderLine->quantity / $orderLine->unit_by_can) }}</td>
                                    <td>{{ numberFormatVnd($orderLine->price) }}</td>
                                    <td>
                                        <strong>{{ numberFormatVnd($orderLine->quantity * $orderLine->price) }}</strong>
                                    </td>
                                    <td>{{ $orderLine->note }}</td>
                                    <td>{{ $orderLine->note2 }}</td>
                                    <td>
                                        @if($order->status === qhn_status_draft)
                                            <button class="btn btn-danger btn-sm" wire:click="deleteConfirm({{ $orderLine->id }})">
                                                <i class="{{ qhn_icon_delete }}"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="6" align="center">
                                    <strong>
                                        {{ __('Total') }}
                                    </strong>
                                </td>
                                <td colspan="2">
                                    <strong>
                                        {{ numberFormatVnd($total) }}
                                    </strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {{ $orderLines->links() }}
                    @else
                        <div class="alert alert-primary" role="alert" style="text-align: center">
                            {{ __('Can not found products') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelProductChange', function () {
            if($('#modalProduct').length > 0){
                $('#modalProduct').modal('show');
            }
        })
    </script>
@endpush


