<div>
    @if (!$check)
        <div class="alert alert-danger mb-0" role="alert">
            {{ __('Request reset password invalid') }}
        </div>
    @else
        @if($user === null)
            <div class="alert alert-danger mb-0" role="alert">
                {{ __('Email invalid') }}
            </div>
        @elseif(time() > strtotime($user->email_verified_at))
            <div class="alert alert-danger mb-0" role="alert">
                {{ __('Time forgot password expired') }}
            </div>
        @else
            <form class="form-horizontal" wire:submit.prevent="submit">
                <div class="row">
                    <div class="col-md-12">
                        {!! renderInputLivewire($errors, 'password', ['type' => 'password']) !!}
                    </div>
                    <div class="col-md-12">
                        {!! renderInputLivewire($errors, 'password_confirmation', ['label' => __('Password confirmation'), 'type' => 'password']) !!}
                    </div>
                    <div class="col-md-12">
                        <div class="mt-4">
                            <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Send</button>
                        </div>
                    </div>
                </div>
            </form>
        @endif
    @endif
</div>
