<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>
    <form class="form-horizontal" wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{ __('Email') }}</label>
                    <div>
                        <input wire:model="email" type="text" class="form-control @error('email') 'parsley-error' @enderror" placeholder="{{ __('Enter Email') }}" maxlength="50" />
                    </div>
                    @error('email')
                        <ul class="parsley-errors-list filled">
                            <li class="parsley-required">{{ $message }}</li>
                        </ul>
                    @enderror
                    @if (session()->has('error_login'))
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="parsley-errors-list filled">
                                    <li class="parsley-required">{{ session('error_login') }}</li>
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>{{ __('Password') }}</label>
                    <div>
                        <input wire:model="password" type="password" class="form-control @error('password') 'parsley-error' @enderror" placeholder="{{ __('Enter password') }}" maxlength="50" />
                    </div>
                    @error('password')
                    <ul class="parsley-errors-list filled">
                        <li class="parsley-required">{{ $message }}</li>
                    </ul>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox">
                            <input checked="checked" type="checkbox" class="custom-control-input" id="customControlInline">
                            <label class="custom-control-label" for="customControlInline">Remember me</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right mt-3 mt-md-0">
                            <a href="{{ route('auth.forgot-password') }}" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                </div>
            </div>
        </div>
    </form>
</div>
