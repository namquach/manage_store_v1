<div>
    <div wire:loading wire:target="submit">
        <x-loading />
    </div>

    <form class="form-horizontal" wire:submit.prevent="submit">
        <div class="row">
            @if($isSend)
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        {{ __('Send mail success, you can get request reset password about 5-10 minutes on this mail') }}
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                {!! renderInputLivewire($errors, 'email', ['placeholder' => 'Enter email']) !!}
            </div>
            <div class="col-md-12">
                <div class="mt-4">
                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">{{ __('Send') }}</button>
                </div>
            </div>
        </div>

    </form>
</div>
