<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar">
                <div class="btn-group focus-btn-group">
                    <a href="{{ route('order.create', ['tmp' => qhn_uniq_id()]) }}" class="{{ qhn_class_btn_create }}">
                        <i class="{{ qhn_icon_create }}"></i> {{ __('Create') }}
                    </a>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectFilterStatus() !!}
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if ($orders->count() > 0)
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Customer Name') }}</th>
                            <th>{{ __('Customer Address') }}</th>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Updated') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td style="width: 4%; text-align: center">{{ $order->id }}</td>
                                <td>{{ $order->customer_name }}</td>
                                <td>{{ $order->customer_address }}</td>
                                <td>{{ $order->date }}</td>
                                <td>
                                    {!! renderButtonStatus($order->status) !!}
                                </td>
                                <td>{{ $order->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('order.update', ['id' => $order->id]) }}" class="{{ qhn_class_btn_edit }}">
                                        <i class="{{ qhn_icon_edit }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm({{ $order->id }})">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $orders->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        {{ __('Can not found orders') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

