<div>
    @if($stateModalCustomer)
        @livewire('customer.modal-customer-list')
    @endif
    <form wire:submit.prevent="submit">
        <div wire:loading>
            <x-loading />
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="card-header bg-transparent border-bottom mt-0">
                    <a href="{{ route('order.index') }}" class="btn btn-secondary" type="button">{{ __('Back') }}</a>
                    @if($order->status === qhn_status_draft)
                        <button class="btn btn-info">{{ __('Save') }}</button>
                        @if (!$isCreate)
                            <button wire:click="updateStatusOrder" class="btn btn-success" type="button">{{ __('Completed') }}</button>
                        @endif
                    @endif
                </h5>
                <br />
            </div>
        </div>

        <input type="hidden" wire:model="order.customer_id" />

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <label for="">{{ __('Customer Name') }}</label>
                    <div class="input-group mb-2">
                        <input wire:model="order.customer_name" type="text" class="form-control" placeholder="{{ __('Choose customer') }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text btn btn-primary" wire:click="eventOpenModalCustomer">
                                <i class="far fa-user"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 15px">
                    {!! renderTextareaLivewire($errors, 'order.customer_address', ['label' => __('Customer Address')]) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    {!! renderInputLivewire($errors, 'order.date', ['label' => __('Date'), 'type' => 'date']) !!}
                </div>
                <div class="col-md-12">
                    {!! renderTextareaLivewire($errors, 'order.note', ['label' => __('Note')]) !!}
                </div>
            </div>
        </div>
    </form>

    <div>
        @livewire('order-line.order-line-table', ['order' => $order])
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('event.stateModelCustomerChange', function () {
            if($('#modalCustomer').length > 0){
                $('#modalCustomer').modal('show');
            }
        })
    </script>
@endpush
