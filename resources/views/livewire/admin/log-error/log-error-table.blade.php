<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-rep-plugin">
        <div class="table-wrapper">
            <div class="btn-toolbar" style="margin-bottom: 30px;">
                <div class="btn-group focus-btn-group">
                    <button class="btn btn-danger" wire:click="deleteConfirm('')">{{ __('Delete selected') }}</button>
                </div>
                <div class="btn-group dropdown-btn-group pull-right">
                    {!! renderSelectPerPage() !!}
                    {!! renderInputSearch() !!}
                </div>
            </div>
            <div class="table-responsive mb-0" data-pattern="priority-columns">
                @if($logErrors->count())
                    <table id="tech-companies-1" class="table table-striped">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" wire:click="toggleCheckAll" {{ $selectAll ? "checked" : "" }} >
                            </th>
                            <th style="text-align: center;">#</th>
                            <th>{{ __('Message') }}</th>
                            <th>{{ __('Date') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($logErrors as $logError)
                            <tr>
                                <td>
                                    <input
                                        type="checkbox"
                                        id="{{ $logError->id }}"
                                        name="{{ $logError->id }}"
                                        class="qhn-checkbox"
                                        value="{{ $logError->id }}"
                                        {{ $selectAll ? "checked" : "" }}
                                        wire:model="checkAllData"
                                    />
                                </td>
                                <td style="width: 4%; text-align: center">{{ $logError->id }}</td>
                                <td>{{ $logError->message }}</td>
                                <td>{{ $logError->created_at }}</td>
                                <td>
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('admin.log-error.show', ['id' => $logError->id]) }}">
                                        <i class="{{ qhn_icon_show }}"></i>
                                    </a>
                                    <button type="button" class="{{ qhn_class_btn_delete }}" wire:click="deleteConfirm('{{ $logError->id }}')">
                                        <i class="{{ qhn_icon_delete }}"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $logErrors->links() !!}
                @else
                    <div class="alert alert-primary" role="alert" style="text-align: center">
                        {{ __('Can not found log errors') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

@push('admin.scripts')
    <script>

    </script>
@endpush

