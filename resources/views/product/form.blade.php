@extends('layout.master')

@section('breadcrumb')
    <h4 class="page-title mb-1">{{ __('Product') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('product-group.index') }}">{{ __('Product') }}</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('product.product-form', ['product' => $product])
                </div>
            </div>
        </div>
    </div>
@endsection
