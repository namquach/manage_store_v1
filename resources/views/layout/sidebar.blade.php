<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li>
                    <a href="{{ route('dashboard.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="uim uim-schedule"></i></div>
                        <span>{{ __('Dashboard') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('customer.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fas fa-user-friends"></i></div>
                        <span>{{ __('Customers') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('supplier.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fa fa-truck fa-flip-horizontal"></i></div>
                        <span>{{ __('Suppliers') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('product.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fa fa-shopping-cart"></i></div>
                        <span>{{ __('Product') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('product-group.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fa fa-shopping-cart"></i></div>
                        <span>{{ __('Product Group') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('setting-price.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fa fa-cogs"></i></div>
                        <span>{{ __('Setting Price') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('order.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fab fa-chrome"></i></div>
                        <span>{{ __('Order') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('purchase.index') }}" class=" waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="fab fa-chrome"></i></div>
                        <span>{{ __('Purchase') }}</span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- Sidebar -->
    </div>
</div>
