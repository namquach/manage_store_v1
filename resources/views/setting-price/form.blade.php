@extends('layout.master')

@section('breadcrumb')
    <h4 class="page-title mb-1">{{ __('Setting Price') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('setting-price.index') }}">{{ __('Setting Price') }}</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('setting-price.setting-price-form', ['settingPrice' => $settingPrice, 'isCreate' => $isCreate])
                </div>
            </div>
        </div>
    </div>
@endsection
