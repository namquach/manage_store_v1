@extends('layout.master')

@section('breadcrumb')
    <h4 class="page-title mb-1">{{ __('Customer') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active">{{ __('Customer') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('customer.customer-table')
                </div>
            </div>
        </div>
    </div>
@endsection
