@extends('layout.master')

@section('breadcrumb')
    <h4 class="page-title mb-1">{{ __('Order') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('order.index') }}">{{ __('Order') }}</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('order.order-form', ['order' => $order, 'isCreate' => $isCreate])
                </div>
            </div>
        </div>
    </div>
@endsection
