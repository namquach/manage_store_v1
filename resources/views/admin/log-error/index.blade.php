@extends('admin-layout.master')

@section('admin.breadcrumb')
    <h4 class="page-title mb-1">{{ __('Log Errors') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('Log Errors') }}</li>
    </ol>
@endsection

@section('admin.content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    @livewire('admin.log-error.log-error-table')
                </div>
            </div>
        </div>
    </div>
@endsection
