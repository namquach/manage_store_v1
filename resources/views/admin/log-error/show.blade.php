@extends('admin-layout.master')

@section('admin.breadcrumb')
    <h4 class="page-title mb-1">{{ __('Show Log Error') }}</h4>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('order.index') }}">{{ __('Show Log Error') }}</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
@endsection

@section('admin.content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-4">
                            <label>User</label>
                        </div>
                        <div class="col-md-8">
                            @php
                                try {
                                    echo $user->name;
                                }catch (Exception $exception) { echo '-'; }
                            @endphp
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Date</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->created_at }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Message</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->message }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Line</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->line }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Code</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->code }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>File</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->file }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Trace</label>
                        </div>
                        <div class="col-md-8">
                            {{ $logError->trace_as_string }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
