<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductUnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_units')->insert([
           ['id' => 1, 'name' => 'Thùng'],
           ['id' => 2, 'name' => 'Chai'],
           ['id' => 3, 'name' => 'Gói'],
        ]);
    }
}
