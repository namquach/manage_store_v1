<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'qhnam.67@gmail.com',
            'password' => \Hash::make('123456'),
            'name' => 'Super Admin',
            'role' => 100
        ]);

        DB::table('users')->insert([
            'email' => 'qhnam.96@gmail.com',
            'password' => \Hash::make('123456'),
            'name' => 'Quach Hoai Nam',
            'role' => 20
        ]);
    }
}
