<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductGroup;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 300; $i++)
        {
            try {
                \DB::table('products')->insert([
                    'sku' => 'qhn.' . $i,
                    'name' => $faker->name,
                    'price' => \Arr::random([1000, 2000, 3000, 4000, 5000]),
                    'vat' => \Arr::random([1, 2, 3, 4, 5, 6, 7]),
                    'quantity_container_by_can' => 20,
                    'group1' => ProductGroup::query()->where(['group' => 1])->first()->id,
                    'group2' => ProductGroup::query()->where(['group' => 2])->first()->id,
                    'group3' => ProductGroup::query()->where(['group' => 3])->first()->id,
                    'note' => $faker->word,
                    'user_id' => 2
                ]);
            }catch (\Exception $exception) {

            }
        }
    }
}
