<?php

namespace Database\Seeders;

use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $provinces  = Province::all();
        $districts = District::all();
        $wards = Ward::all();
        for($i = 0; $i < 119; $i++)
        {
            try {
                $_province = $provinces->random();
                $_district = $districts->where('parent_code', $_province->code)->random();
                $_ward = $wards->where('parent_code', $_district->code)->random();
                \DB::table('customers')->insert([
                    'name' => $faker->firstName . ' ' . $faker->lastName,
                    'address' => $faker->address,
                    'province' => $_province->name,
                    'district' => $_district->name,
                    'ward' => $_ward->name,
                    'user_id' => 2
                ]);
            }catch (\Exception $exception) { }

            echo ($i + 1) . ' Create customer success' . PHP_EOL;
        }
    }
}
