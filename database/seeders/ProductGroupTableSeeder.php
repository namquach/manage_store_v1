<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 1000; $i++){
            \DB::table('product_groups')->insert([
                'name' => 'Group ' . $i,
                'user_id' => 2,
                'group' => \Arr::random([1, 2, 3]),
            ]);
        }
    }
}
