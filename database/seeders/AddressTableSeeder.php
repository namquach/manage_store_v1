<?php

namespace Database\Seeders;

use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = base_path() . '/database/vietnam_dataset_address/';
        $listFileJson = scandir($path);
        $listFileJson = array_diff($listFileJson, ['.', '..']);

        $counter = 1;

        foreach ($listFileJson as $fileJson)
        {
            $contentFileJson = json_decode(file_get_contents($path . $fileJson), true);

            $province = new Province();
            $province->name = $contentFileJson['name'];
            $province->code = $contentFileJson['code'];
            $province->save();

            if(empty($contentFileJson['district'])) continue;

            foreach($contentFileJson['district'] as $districtItem)
            {
                $district = new District();
                $district->name = $districtItem['name'];
                $district->prefix = $districtItem['pre'];
                $district->code = $province->code . '-' . \Str::slug($district->name);
                $district->parent_code = $province->code;
                $district->save();

                if(empty($districtItem['ward'])) continue;

                $dataInsertWards = [];

                foreach ($districtItem['ward'] as $key => $wardItem)
                {
                    $dataInsertWards[] = [
                        'name' => $wardItem['name'],
                        'prefix' => $wardItem['pre'],
                        'parent_code' => $district->code,
                    ];
                    //$ward = new Ward();
                    //$ward->name = $wardItem['name'];
                    //$ward->prefix = $wardItem['pre'];
                    //$ward->parent_code = $district->code;
                    //$ward->save();
                }

                \DB::table('wards')->insert($dataInsertWards);
                echo  ($counter).' Insert success' . PHP_EOL;
                $counter = $counter + 1;
            }
        }

    }
}
