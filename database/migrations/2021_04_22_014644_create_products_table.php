<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('group1');
            $table->integer('group2');
            $table->integer('group3');
            $table->string('sku');
            $table->string('name',255);
            $table->decimal('price', 12, 2)->default(0);
            $table->float('vat')->default(0);
            $table->integer('quantity_container_by_can')->default(1);
            $table->string('note', 500)->default('')->nullable(true);
            $table->integer('stock')->default(0);
            $table->integer('unit')->default(1);
            $table->integer('user_id');
            $table->timestamp('created_at')->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
