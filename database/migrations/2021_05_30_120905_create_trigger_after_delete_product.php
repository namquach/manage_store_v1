<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriggerAfterDeleteProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::unprepared('
                CREATE
                DEFINER=root@localhost
                TRIGGER trigger_after_delete_product AFTER DELETE ON products
                FOR EACH ROW
                    BEGIN
                        DELETE FROM setting_price_products WHERE product_id = OLD.id;
                    END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::unprepared('DROP TRIGGER IF EXISTS setting_prices_after_insert;');
    }
}
