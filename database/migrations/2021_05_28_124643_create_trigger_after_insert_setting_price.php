<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriggerAfterInsertSettingPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            \Illuminate\Support\Facades\DB::unprepared('
                CREATE
                DEFINER=root@localhost
                TRIGGER setting_prices_after_insert AFTER INSERT ON setting_prices
                FOR EACH ROW
                    BEGIN
                        DECLARE product_id INT;
                        DECLARE done INT DEFAULT FALSE;
                        DECLARE cur CURSOR FOR SELECT id FROM products;
                        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                        OPEN cur;
                            read_loop: LOOP
                                FETCH cur INTO product_id;

                                IF done THEN
                                LEAVE read_loop;
                                END IF;

                                INSERT INTO setting_price_products (`setting_price_id`, `product_id`, `price`) VALUES(NEW.id, product_id, 0);
                            END LOOP;
                        CLOSE cur;

                    END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::unprepared('DROP TRIGGER IF EXISTS setting_prices_after_insert;');
        //Schema::dropIfExists('trigger_after_insert_setting_price');
    }
}
