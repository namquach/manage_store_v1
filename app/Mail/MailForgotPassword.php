<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class MailForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User $user */
    protected $user;
    protected $link;

    public function __construct(User $user)
    {
        $this->user = $user;

        $this->link = route('auth.update-forgot-password', [
            'email' => $user->email,
            'hash' => \Hash::make($user->encryption),
        ]);
    }


    public function build()
    {
        $this->subject('Mail reset password');
        return $this->view('mail.forgot-password', [
            'user' => $this->user,
            'link' => $this->link,
        ]);
    }
}
