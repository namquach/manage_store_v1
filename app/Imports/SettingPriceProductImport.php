<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\SettingPrice;
use App\Models\SettingPriceProduct;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SettingPriceProductImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;
    /** @var SettingPrice $settingPrice  */
    private $settingPrice;
    public function __construct(SettingPrice $settingPrice)
    {
        $this->settingPrice = $settingPrice;
    }

    public function rules(): array
    {
        return [
            'sku' => 'nullable',
            'name' => 'nullable',
            'price' => 'nullable',
        ];
    }

    public function model(array $row)
    {
        $product = Product::query()->firstWhere(['sku' => $row['sku']]);

        if($product === null) return dd('Sku can not null or invalid, please check again !');

        $model = SettingPriceProduct::query()->where([
            'product_id' => $product->id,
            'setting_price_id' => $this->settingPrice->id,
        ])->first();

        if($model && (float)$row['price'] > 0){
            $model->price = (float)$row['price'];
            $model->save();
        }

        return $model;
    }
}
