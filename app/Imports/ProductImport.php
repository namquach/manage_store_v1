<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductUnit;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable, SkipsFailures, SkipsErrors;

    public function rules(): array
    {
        return [
            'name' => 'required|max:150|min:1',
            'sku' => 'required|max:150|min:3',
            'vat' => 'numeric|between:0,100',
            'group1' => 'required',
            'group2' => 'required',
            'group3' => 'required',
            'price' => 'numeric',
            'quantity_container_by_can' => 'integer',
            'unit' => 'required',
            'note' => '',
        ];
    }


    public function model(array $row)
    {
        $group1 = ProductGroup::query()->firstOrCreate(['name' => $row['group1'], 'group' => 1, 'user_id' => Auth::id()]);
        $group2 = ProductGroup::query()->firstOrCreate(['name' => $row['group2'], 'group' => 2, 'user_id' => Auth::id()]);
        $group3 = ProductGroup::query()->firstOrCreate(['name' => $row['group3'], 'group' => 3, 'user_id' => Auth::id()]);

        $productUnit = ProductUnit::query()->firstOrCreate(['name' => $row['unit']]);

        $product = Product::query()->where('sku', $row['sku'])->first();
        if($product === null){
            $product = new Product();
            $product->sku = $row['sku'];
        }

        $product->name = $row['name'];
        $product->price = $row['price'];
        $product->vat = $row['vat'];
        $product->unit = $productUnit->id;
        $product->quantity_container_by_can = $row['quantity_container_by_can'];
        $product->group1 = $group1->id;
        $product->group2 = $group2->id;
        $product->group3 = $group3->id;
        $product->note = $row['note'];
        $product->user_id = \Auth::id();
        $product->save();

        return $product;
    }


}
