<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\Order;
use App\Models\OrderLine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        return view('order.index');
    }

    public function create(Request $request)
    {

        return view('order.form', [
            'title' => __('Create'),
            'order' => Order::query()->firstOrCreate([
                'tmp' => $request->get('tmp'),
                'status' => qhn_status_draft,
                'user_id' => \Auth::user()->id,
            ]),
            'isCreate' => true,
        ]);
    }

    public function update($id)
    {
        $order = Order::query()->where(['id' => $id, 'user_id' =>  Auth::user()->id])->first();

        if($order === null) abort(404);

        return view('order.form', [
            'title' => __('Create'),
            'order' => $order,
            'isCreate' => false,
        ]);
    }
}
