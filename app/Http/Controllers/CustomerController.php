<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\Customer;
use App\Models\LogError;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        return view('customer.index');
    }

    public function create(){
        return view('customer.form', [
            'title' => __('Create'),
            'customer' => new Customer(),
        ]);
    }

    public function update($id){
        $customer = Customer::query()->where([
            'id' => $id,
            'user_id' => \Auth::user()->id,
        ])->first();

        if($customer === null) abort(404);

        return view('customer.form', [
            'title' => __('Update'),
            'customer' => $customer,
        ]);
    }

    public function search(Request $request)
    {
        return response()->json(
            empty($request->get('search')) ? []
                : Customer::searchSelect2($request->get('search'))->limit(10)->get(),
        );
    }
}
