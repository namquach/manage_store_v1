<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\SettingPrice;
use Illuminate\Http\Request;

class SettingPriceController extends Controller
{
    public function index()
    {
        return view('setting-price.index');
    }

    public function update($id)
    {
        $settingPrice = SettingPrice::query()->where([
            'id' => $id,
            'user_id' => \Auth::user()->id,
        ])->first();

        if($settingPrice === null) abort(404);

        return view('setting-price.form', [
            'settingPrice' => $settingPrice,
            'title' => __('Update Setting Price'),
            'isCreate' => false,
        ]);
    }

    public function create(Request $request)
    {
        return view('setting-price.form', [
            'settingPrice' => SettingPrice::query()->firstOrCreate([
                'tmp' => $request->get('tmp'),
                'user_id' => \Auth::user()->id,
            ]),
            'title' => __('Create Setting Price'),
            'isCreate' => true,
        ]);
    }
}
