<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\LogError;
use App\Models\User;

class AdminLogErrorController extends Controller
{
    public function index()
    {
        return view('admin.log-error.index');
    }

    public function show($id)
    {
        $logError = LogError::query()->where(['_id' => $id])->first();
        if($logError === null) abort(404);

        $user = User::query()->where('id', $logError->user_id)->first();
        return view('admin.log-error.show', [
            'logError' => $logError,
            'title' => __('Show Log Error'),
            'user' => $user,
        ]);
    }
}
