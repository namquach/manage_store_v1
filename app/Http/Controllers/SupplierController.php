<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\Supplier;

class SupplierController extends Controller
{
    public function index()
    {
        return view('supplier.index');
    }

    public function create()
    {
        return view('supplier.form', [
            'title' => __('Create'),
            'supplier' => new Supplier(),
            'isCreate' => true,
        ]);
    }

    public function update($id)
    {
        $supplier =  Supplier::query()->where([
            'id' => $id,
            'user_id' => \Auth::user()->id,
        ])->first();

        if ($supplier === null) abort(404);

        return view('supplier.form', [
            'title' => __('Create'),
            'supplier' => $supplier,
            'isCreate' => false,
        ]);
    }
}
