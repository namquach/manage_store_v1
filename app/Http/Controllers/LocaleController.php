<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocaleController extends Controller
{
    public function switch($language)
    {
        if(!in_array($language, ['en', 'vn'])) abort(400);

        $oldUrl = empty($_SERVER['HTTP_REFERER']) ? route('dashboard.index') : $_SERVER['HTTP_REFERER'];

        App::setLocale($language);

        setcookie('locale', $language, time() + (86400 * 180), '/'); // 6 month

        return redirect($oldUrl);
    }
}
