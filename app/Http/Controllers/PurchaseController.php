<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\Purchase;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function index()
    {
        return view('purchase.index');
    }

    public function create(Request $request)
    {
        return view('purchase.form', [
            'title' => __('Create'),
            'purchase' => Purchase::query()->firstOrCreate([
                'tmp' => $request->get('tmp'),
                'status' => qhn_status_draft,
                'user_id' => \Auth::user()->id,
            ]),
            'isCreate' => true,
        ]);
    }

    public function update($id)
    {
        $purchase = Purchase::query()->where([
            'id' => $id,
            'user_id' => \Auth::user()->id
        ])->first();

        if($purchase === null) abort(404);

        return view('purchase.form', [
            'title' => __('Create'),
            'purchase' => $purchase,
            'isCreate' => false,
        ]);
    }
}
