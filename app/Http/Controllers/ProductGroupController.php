<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\ProductGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductGroupController extends Controller
{
    public function index()
    {
        return view('product-group.index');
    }

    public function create()
    {
        return view('product-group.form', [
            'productGroup' => new ProductGroup(),
            'title' => __('Create'),
        ]);
    }

    public function update($id)
    {
        $productGroup = ProductGroup::query()->where(['id' => $id, 'user_id' => Auth::user()->id])->first();

        if($productGroup === null) abort(404);

        return view('product-group.form', [
            'productGroup' => $productGroup,
            'title' => __('Update'),
        ]);
    }
}
