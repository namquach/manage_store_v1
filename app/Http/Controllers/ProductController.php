<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('product.index');
    }

    public function create()
    {
        return view('product.form', [
            'product' => new Product(),
            'title' => __('Create'),
        ]);
    }

    public function update($id)
    {
        $product = Product::query()->where(['id'=> $id, 'user_id' => \Auth::user()->id])->first();

        if($product === null) abort(404);

        return view('product.form', [
            'product' => $product,
            'title' => __('Update'),
        ]);
    }

    public function search(Request $request)
    {
        return response()->json(
            empty($request->get('search')) ? []
                : Product::searchAutocomplete($request->get('search'))->limit(10)->get()
        );
    }
}
