<?php

namespace App\Http\Controllers;

use App\Components\Controller;
use App\Mail\MailForgotPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('auth.login');
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function updateForgotPassword(Request $request)
    {
        return view('auth.update-password', [
            'request' => $request->toArray()
        ]);
    }
}
