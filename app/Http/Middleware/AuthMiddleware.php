<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if(\Auth::check() && Auth::user()->role === qhn_role_client)
        {
            return $next($request);

        }

        return redirect()->route('auth.login');
    }
}
