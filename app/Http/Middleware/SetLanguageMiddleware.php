<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetLanguageMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        \App::setLocale(!empty($_COOKIE['locale']) ? $_COOKIE['locale'] : config('app.locale'));
        return $next($request);
    }
}
