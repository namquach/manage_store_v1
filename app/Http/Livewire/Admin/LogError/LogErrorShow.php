<?php

namespace App\Http\Livewire\Admin\LogError;

use Livewire\Component;

class LogErrorShow extends Component
{
    public function render()
    {
        return view('livewire.admin.log-error.log-error-show');
    }
}
