<?php

namespace App\Http\Livewire\Admin\LogError;

use App\Components\Component;
use App\Models\LogError;
use App\Traits\FilterTable;

class LogErrorTable extends Component
{
    use FilterTable;

    public $listeners = ['delete'];

    public function render()
    {
        return view('livewire.admin.log-error.log-error-table', [
            'logErrors' => LogError::search($this->search)->paginate((int)$this->perPage),
        ]);
    }

    public function toggleCheckAll()
    {
        $this->selectAll = !$this->selectAll;

        if($this->selectAll){
            $logErrors = LogError::search($this->search)->skip(((int)$this->page -1) * (int)$this->perPage)->take((int)$this->perPage)->pluck('_id')->toArray();
            $this->checkAllData = $logErrors;
        }else{
            $this->checkAllData = [];
        }
    }

    public function setPage($page)
    {
        $this->page = $page;
        $this->selectAll = false;
    }

    public function delete($id){
        $id = !is_array($id) ? [$id] : $id;
        LogError::query()->whereIn('_id', $id)->delete();
        $this->page = 1;
        $this->checkAllData = [];
        $this->selectAll = false;
        $this->dispatchAlertify();
    }

    public function deleteConfirm($id): void
    {
        if(empty($id)){
            $id = $this->checkAllData;
        }

        $this->dispatchBrowserEvent('swal.confirm', [
            'type' => 'warning',
            'title' => 'Are you sure ?',
            'id' => $id,
            'text' => '',
            'icon' => 'warning',
        ]);
    }
}
