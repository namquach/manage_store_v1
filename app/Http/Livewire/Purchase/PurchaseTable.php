<?php

namespace App\Http\Livewire\Purchase;

use App\Components\Component;
use App\Models\Purchase;
use App\Traits\DispatchBrowserEventTrait;
use App\Traits\FilterTable;

class PurchaseTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    public $stateModal = false;

    public $status;

    public $listeners = ['delete'];

    public function render()
    {
        $purchaseProvider = Purchase::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc');

        if(!empty($this->status)) $purchaseProvider->where('status', $this->status);

        return view('livewire.purchase.purchase-table', [
            'purchases' => $purchaseProvider->paginate($this->perPage)
        ]);
    }

    public function delete($id)
    {
        $purchase = Purchase::query()->firstOrFail([
            'id' => $id,
            'user_id' => \Auth::user()->id,
        ]);

        if($purchase->status == qhn_status_completed){
            $this->dispatchBrowserAlertify('warning', 'Purchase completed, can not delete');
            return false;
        }

        $purchase->delete();
        $this->dispatchBrowserAlertify('success', 'Delete purchase success');
    }
}
