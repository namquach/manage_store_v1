<?php

namespace App\Http\Livewire\Purchase;

use App\Components\Component;
use App\Models\Purchase;
use App\Models\PurchaseLine;
use App\Models\Supplier;
use App\Traits\DispatchBrowserEventTrait;

class PurchaseForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Purchase $purchase */
    public $purchase;

    public $isCreate;

    public $stateModalSupplier = false;

    public $listeners = ['eventChooseSupplier', 'hideModal'];

    public function rules()
    {
        return [
            'purchase.supplier_id' => 'required',
            'purchase.supplier_name' => 'required|max:50',
            'purchase.supplier_address' => 'required|max:250',
            'purchase.date' => 'required',
            'purchase.note' => '',
        ];
    }

    public function render()
    {
        return view('livewire.purchase.purchase-form');
    }

    public function eventOpenModalSupplier()
    {
        $this->stateModalSupplier = true;
        $this->dispatchBrowserEvent('event.stateModelSupplierChange');
    }

    public function eventChooseSupplier($supplier)
    {
        $this->purchase->supplier_id = $supplier['id'];
        $this->purchase->supplier_name = $supplier['name'];
        $this->purchase->supplier_address = Supplier::fullAddress($supplier);
        $this->dispatchBrowserEvent('event.closeModal', ['modal' => 'stateModalSupplier']);
    }

    public function submit()
    {
        $this->validate();

        $checkPurchaseLine = PurchaseLine::query()->where('purchase_id', $this->purchase->id)->get();

        if($checkPurchaseLine->count() === 0){
            $this->dispatchBrowserEvent('alertify.confirm.error', [
                'message' => __('Products can not be null'),
            ]);
            return false;
        }

        $this->purchase->tmp = '';
        $this->purchase->save();

        $this->dispatchBrowserEvent('alertify.confirm.success', [
            'message' => __('Create purchase success'),
        ]);

        return redirect()->route('purchase.index');
    }

    public function updateStatusPurchase()
    {
        if($this->purchase->status !== qhn_status_draft){
            $this->dispatchBrowserAlertify('warning', 'Purchase completed, can not update status');
            return false;
        }

        $purchaseLines = PurchaseLine::query()->with(['product'])->where('purchase_id', $this->purchase->id)->get();

        if($purchaseLines->count() === 0){
            $this->dispatchBrowserAlertify('warning', 'Purchase must have product, can not update status');
            return false;
        }

        $this->purchase->status = qhn_status_completed;
        $this->purchase->save();

        foreach($purchaseLines as $purchaseLine)
        {
            $product = $purchaseLine->product;

            $product->stock = $product->stock + $purchaseLine->quantity;
            $product->store_in = $product->store_in + $purchaseLine->quantity;
            $product->save();
        }

        $this->dispatchBrowserAlertify('success', 'Completed status purchase success');
        $this->emit('reloadComponent');
    }

    public function hideModal()
    {
        $this->stateModalSupplier = false;
    }
}
