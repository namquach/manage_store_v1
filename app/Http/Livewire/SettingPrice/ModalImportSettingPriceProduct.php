<?php

namespace App\Http\Livewire\SettingPrice;

use App\Imports\SettingPriceProductImport;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ModalImportSettingPriceProduct extends Component
{
    use WithFileUploads;
    public $file;
    public $label = 'Choose file';

    public $listeners = ['hideModal'];

    public $settingPrice;

    public function rules()
    {
        return [
            'file' => 'file|mimes:xls,xlsx'
        ];
    }

    public function submit()
    {
        if(empty($this->file)){
            session()->flash('import_product_error', 'File invalid or value invalid');
            return false;
        }

        try {
            //Excel::import(new ProductImport(),$this->file->getRealPath());
            Excel::import(new SettingPriceProductImport($this->settingPrice), $this->file->getRealPath());
        }
        catch (\Exception $exception){
            session()->flash('import_product_error', 'File invalid or value invalid');
            return false;
        }

        session()->flash('import_product_success', 'Import product success');
    }

    public function hideModal()
    {
        $this->emit('componentRefreshSettingProductTable');
    }

    public function render()
    {
        if(!empty($this->file)) $this->label = $this->file->getClientOriginalName();

        return view('livewire.setting-price.modal-import-setting-price-product');
    }
}
