<?php

namespace App\Http\Livewire\SettingPrice;

use App\Models\SettingPriceCustomer;
use App\Traits\DispatchBrowserEventTrait;
use Livewire\Component;

class SettingPriceCustomerForm extends Component
{
    use DispatchBrowserEventTrait;

    public $settingPrice;

    public $listeners = ['eventAddSettingPriceCustomer', 'eventSaveSettingPriceCustomers', 'delete'];

    public function rules()
    {
        return [
            'customers' => ''
        ];
    }

    public function render()
    {
        return view('livewire.setting-price.setting-price-customer-form', [
            'customers' => SettingPriceCustomer::getBySettingPrice($this->settingPrice->id)
        ]);
    }

    public function eventAddSettingPriceCustomer($customer)
    {
        $customer = json_decode($customer, true);

        $check = SettingPriceCustomer::query()->firstWhere(['customer_id' => $customer['id']]);

        if($check && $this->settingPrice->tmp == ''){
            $this->dispatchBrowserAlertify('warning', 'Customer used, can not add customer');

            return false;
        }

        SettingPriceCustomer::query()->updateOrCreate([
            'customer_id' => $customer['id'],
            'setting_price_id' => $this->settingPrice->id
        ]);

        $this->dispatchBrowserAlertify('success', 'Add customer success');
        //$this->dispatchBrowserEvent('event.autoFocusSelect2Customer');
    }

    public function eventSaveSettingPriceCustomers()
    {
//        $data = [];
//
//        foreach($this->customers as $customer){
//            $data[] = [
//                'customer_id' => $customer['id'],
//                'setting_price_id' => $this->settingPriceId,
//            ];
//        }
//        SettingPriceCustomer::query()->upsert($data, [], []);
    }

    public function deleteSettingPriceCustomer($settingPriceCustomer)
    {
        $this->dispatchBrowserEvent('swal.confirm', [
            'type' => 'warning',
            'title' => 'Are you sure ?',
            'id' => join(',', [$settingPriceCustomer['customer_id'], $settingPriceCustomer['setting_price_id']]),
            'text' => '',
            'icon' => 'warning',
        ]);
    }

    public function delete($id)
    {
        list($customer_id, $setting_price_id) = explode(',', $id);

        SettingPriceCustomer::query()->where([
            'customer_id' => $customer_id,
            'setting_price_id' => $setting_price_id,
        ])->limit(1)->delete();

        $this->dispatchBrowserAlertify('success', 'Delete customer success');
    }
}
