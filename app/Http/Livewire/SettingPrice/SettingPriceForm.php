<?php

namespace App\Http\Livewire\SettingPrice;

use App\Components\Component;
use App\Models\SettingPrice;

class SettingPriceForm extends Component
{
    /** @var SettingPrice $settingPrice */
    public $settingPrice;

    public function rules()
    {
        return [
            'settingPrice.name' => 'required|max:150|min:3|unique:setting_prices,name,' . $this->settingPrice->id,
        ];
    }

    public function render()
    {
        return view('livewire.setting-price.setting-price-form');
    }

    public function submit()
    {
        $this->validate();

        $this->settingPrice->tmp = '';
        $this->settingPrice->save();

        return redirect()->route('setting-price.index');
    }
}
