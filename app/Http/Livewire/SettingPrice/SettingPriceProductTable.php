<?php

namespace App\Http\Livewire\SettingPrice;

use App\Components\Component;
use App\Models\SettingPriceProduct;
use App\Traits\DispatchBrowserEventTrait;
use App\Traits\FilterTable;

class SettingPriceProductTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    public $settingPrice;

    public $stateModalImport = false;

    public $listeners = [
        'componentRefreshSettingProductTable'
    ];

    public function render()
    {
        return view('livewire.setting-price.setting-price-product-table', [
            'settingPriceProducts' => SettingPriceProduct::search($this->settingPrice->id, $this->search)->paginate($this->perPage),
        ]);
    }

    public function componentRefreshSettingProductTable() {}

    public function getQueryString()
    {
        return [];
    }

    public function changeSettingPriceProduct($settingPriceProduct, $price)
    {
        SettingPriceProduct::query()->where(['id' => $settingPriceProduct['id']])->first()->update([
            'price' => (float)$price,
        ]);

        $this->dispatchBrowserAlertify('success', 'Update price success');
    }

    public function updateStateImport()
    {
        $this->stateModalImport = true;
        $this->dispatchBrowserEvent('event.stateImportSettingPriceProduct');

    }
}
