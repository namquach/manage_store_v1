<?php

namespace App\Http\Livewire\SettingPrice;

use App\Models\SettingPrice;
use App\Traits\FilterTable;
use Livewire\Component;
use Livewire\WithPagination;

class SettingPriceTable extends Component
{
    use WithPagination, FilterTable;

    public function render()
    {
        return view('livewire.setting-price.setting-price-table', [
            'settingPrices' => SettingPrice::search($this->search)->paginate($this->perPage),
        ]);
    }
}
