<?php

namespace App\Http\Livewire\PurchaseLine;

use App\Components\Component;
use App\Models\Purchase;
use App\Models\PurchaseLine;
use App\Traits\FilterTable;
use App\Traits\DispatchBrowserEventTrait;

class PurchaseLineTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    /**@var Purchase $purchase */
    public $purchase;

    public $stateModalProduct = false;
    public $total = 0;

    public $listeners = [
        'callbackReceiveProduct',
        'delete',
        'reloadComponent' => '$refresh',
    ];

    public function render()
    {
        $this->total = PurchaseLine::sumTotalPriceByPurchase($this->purchase->id);
        return view('livewire.purchase-line.purchase-line-table', [
            'purchaseLines' => PurchaseLine::search($this->purchase->id, $this->search)->paginate($this->perPage),
        ]);
    }

    public function reloadComponent()
    {

    }

    public function eventOpenModalProduct()
    {
        $this->stateModalProduct = true;
        $this->dispatchBrowserEvent('event.stateModelProductChange');
    }

    public function getQueryString()
    {
        return [];
    }

    public function callbackReceiveProduct($product)
    {
        if($this->purchase->status === qhn_status_completed){
            $this->dispatchBrowserAlertify('error', 'Order have been completed');
        }

        $purchaseLine = PurchaseLine::query()->firstWhere([
            'purchase_id' => $this->purchase->id,
            'product_id' => $product['id']
        ]);

        if(empty($purchaseLine)){
            $purchaseLine = new PurchaseLine();
            $purchaseLine->product_id = $product['id'];
            $purchaseLine->purchase_id = $this->purchase->id;
        }

        $purchaseLine->quantity = $product['quantity'];
        $purchaseLine->price = (float)$product['price'];
        $purchaseLine->save();

        $this->dispatchAlertify('success', 'Add product success');
    }

    public function delete($id){
        if($this->purchase->status === qhn_status_completed){
            $this->dispatchBrowserAlertify('warning', 'Purchase completed, can not delete product');
            return false;
        }

        PurchaseLine::query()->where('id', $id)->delete();
        $this->dispatchBrowserAlertify('success', 'Delete product success');

    }
}
