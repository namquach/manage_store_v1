<?php

namespace App\Http\Livewire\PurchaseLine;

use App\Models\OrderLine;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseLine;
use App\Traits\DispatchBrowserEventTrait;
use Livewire\Component;

class PurchaseLineForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Product $product */
    public $product;

    /** @var Purchase $purchase */
    public $purchase;

    public $sku;
    public $name;
    public $quantity = 0;
    public $price = 0;
    public $product_id;
    public $unit;
    public $unit_by_can = 0;
    public $more_info = [];
    public $productUnit = [];
    public $note;

    public $listeners = [
        'onChangeSku',
    ];

    public function rules()
    {
        return [
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required|gt:0',
            'quantity' => 'required|numeric|gt:0',
            'product_id' => '',
        ];
    }

    public function render()
    {
        if(!empty($this->sku))
        {
            $this->more_info = [
                'unit_by_can' => $this->unit_by_can,
                'exchange_by_can' => $this->unit == 1 ? (int)$this->quantity : qhn_format_number((int)$this->quantity / $this->unit_by_can),
                'quantity' => $this->unit == 1 ? (int)$this->quantity * $this->unit_by_can : (int)$this->quantity,
            ];
        }

        return view('livewire.purchase-line.purchase-line-form');
    }

    public function create()
    {
        $this->validate();

        $purchaseLine = new PurchaseLine();
        $purchaseLine->product_id = $this->product_id;
        $purchaseLine->purchase_id = $this->purchase->id;
        $purchaseLine->price = $this->price;
        $purchaseLine->quantity = $this->unit != 1 ? (int)$this->quantity : (int)$this->quantity * $this->unit_by_can;
        $purchaseLine->note = $this->note;
        $purchaseLine->unit = $this->unit;
        $purchaseLine->unit_by_can = $this->unit_by_can;

        $purchaseLine->save();

        $this->reset([
            'name',
            'sku',
            'price',
            'quantity',
            'note'
        ]);

        $this->dispatchBrowserEvent('event.autofocusSelect2Sku');
        $this->dispatchBrowserAlertify('success', 'Add product success');
        $this->emit('reloadComponent');
    }

    public function onChangeSku($sku)
    {
        $product = Product::findOne('sku', $sku);

        if($product !== null)
        {
            $this->sku = $sku;
            $this->name = $product->name;
            $this->price = calculatePrice($product->price, $product->vat);
            $this->product_id = $product->id;
            $this->unit = $product->productUnit->id;
            $this->unit_by_can = $product->quantity_container_by_can;
            $this->quantity = 0;
            $this->productUnit = [
                $product->productUnit->toArray(),
                ['id' => 1, 'name' => 'Thùng'],
            ];
        }
    }
}
