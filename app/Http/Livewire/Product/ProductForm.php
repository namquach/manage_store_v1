<?php

namespace App\Http\Livewire\Product;

use App\Components\Component;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductUnit;

class ProductForm extends Component
{
    /** @var Product $product */
    public $product;
    public $productGroups;

    public $productUnit = [];

    public function render()
    {
        if(count($this->productUnit) === 0) $this->productUnit = ProductUnit::getAll();

        $this->productGroups = ProductGroup::query()->where('user_id', \Auth::user()->id)->get();

        return view('livewire.product.product-form');
    }

    public function rules()
    {
        return [
            'product.name' => 'required|max:150|min:1',
            'product.sku' => 'required|max:150|min:3|unique:products,sku,' . $this->product->id,
            'product.vat' => 'numeric|between:0,100',
            'product.group1' => 'required',
            'product.group2' => 'required',
            'product.group3' => 'required',
            'product.price' => 'numeric',
            'product.quantity_container_by_can' => 'integer',
            'product.unit' => 'required',
            'product.note' => '',
        ];
    }

    public function submit()
    {
        $this->validate();
        $this->product->save();

        return redirect()->route('product.index');
    }

    public function onChangeGroup($value, $group){
        $this->product->{'group' . $group} = (int)$value;
    }
}
