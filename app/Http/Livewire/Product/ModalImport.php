<?php

namespace App\Http\Livewire\Product;

use App\Components\Component;
use App\Imports\ProductImport;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ModalImport extends Component
{
    use WithFileUploads;
    public $file;
    public $label = 'Choose file';

    public $listeners = ['hideModal'];
    public function rules()
    {
        return [
            'file' => 'file|mimes:xls,xlsx'
        ];
    }

    public function render()
    {
        if(!empty($this->file)) $this->label = $this->file->getClientOriginalName();

        return view('livewire.product.modal-import');
    }


    public function submit()
    {
        try {
            Excel::import(new ProductImport(),$this->file->getRealPath());
        }
        catch (\Exception $exception){
            session()->flash('import_product_error', 'File invalid or value invalid');
            return false;
        }

        session()->flash('import_product_success', 'Import product success');

//        dd($this->file->getRealPath());
    }

    public function hideModal()
    {
        $this->dispatchBrowserEvent('event.closeModal');
    }
}
