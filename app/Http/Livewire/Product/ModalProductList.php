<?php

namespace App\Http\Livewire\Product;

use App\Components\Component;
use App\Models\Product;
use App\Traits\FilterTable;

class ModalProductList extends Component
{
    use FilterTable;

    public $quantity = [];

    public $isOrder;

    public function render()
    {
        return view('livewire.product.modal-product-list', [
            'products' => Product::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }

    public function getQueryString()
    {
        return [];
    }

    public function eventOnChooseProduct($product)
    {
        $product['quantity'] = @(int)$this->quantity[$product['id']];

        if($product['quantity'] <= 0){
            $this->dispatchAlertify('error', 'Product must have quantity');
        }else{
            $this->emit('callbackReceiveProduct', $product);
        }
    }
}
