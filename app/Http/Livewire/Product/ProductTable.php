<?php

namespace App\Http\Livewire\Product;

use App\Components\Component;
use App\Models\OrderLine;
use App\Models\Product;
use App\Models\PurchaseLine;
use App\Traits\FilterTable;

class ProductTable extends Component
{
    use FilterTable;

    protected $listeners = [
        'delete',
        'hideModal'
    ];

    public $stateModalImportProduct = false;

    public function render()
    {
        return view('livewire.product.product-table', [
            'products' => Product::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                                                ->paginate($this->perPage),
        ]);
    }

    public function delete($id)
    {
        $productOrderLine = OrderLine::query()->where('product_id', $id)->first();
        $productPurchaseLine = PurchaseLine::query()->where('product_id', $id)->first();

        if(!empty($productOrderLine) || !empty($productPurchaseLine)){
            $this->dispatchAlertify('error', 'Product have used, can not delete');
            return false;
        }

        Product::query()->where('id', $id)->delete();
        $this->dispatchAlertify();
    }

    public function showModalImportProduct()
    {
        $this->stateModalImportProduct = true;
        $this->dispatchBrowserEvent('event.stateModalImportProduct');
    }

    public function hideModal(){ $this->stateModalImportProduct = false; }

}
