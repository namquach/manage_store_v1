<?php

namespace App\Http\Livewire\Auth;

use App\Jobs\JobSendMail;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class ForgotPasswordForm extends Component
{
    public $email;

    public $isSend = false;

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function render()
    {
        return view('livewire.auth.forgot-password-form');
    }

    public function submit()
    {
        $this->validate();

        $user = User::query()->firstWhere(['email' => $this->email]);
        if($user !== null){
            $user->encryption = qhn_uniq_id();
            $user->email_verified_at = Carbon::now()->addMinutes(25);
            $user->save();

            JobSendMail::dispatch($user)->delay(now()->addMinutes(3));
        }

        $this->isSend = true;
        $this->reset([
            'email'
        ]);
    }
}
