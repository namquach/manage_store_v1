<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;

class UpdateForgotPasswordForm extends Component
{
    public $request;

    public $user;

    public $password;
    public $password_confirmation;
    public $check = false;

    public function rules()
    {
        return [
            'password' => 'required|min:8|confirmed',
        ];
    }

    public function render()
    {
        $this->user = User::query()->firstWhere(['email' => @$this->request['email']]);

        if($this->user !== null){
            $this->check = \Hash::check($this->user->encryption, @$this->request['hash']);
        }

        return view('livewire.auth.update-forgot-password-form');
    }

    public function submit()
    {
        $this->validate();

        $this->user->password = \Hash::make($this->password);
        $this->user->save();

        return redirect()->route('auth.login');
    }
}
