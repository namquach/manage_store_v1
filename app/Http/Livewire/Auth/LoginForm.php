<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class LoginForm extends Component
{
    public $email;
    public $password;

    public function render()
    {
        return view('livewire.auth.login-form');
    }

    public function submit()
    {
        $this->validate(User::ruleLogin());

        if(\Auth::attempt(['email' => $this->email, 'password' => $this->password])){
            if(\Auth::user()->role === qhn_role_admin){
                return redirect()->to(route('admin.log-error.index'));
            }

            if(\Auth::user()->role === qhn_role_client)
                return redirect()->to(route('dashboard.index'));

            return redirect()->to(route('auth.login'));
        }else{
            session()->flash('error_login', __('Email or Password incorrect'));
        }
    }
}
