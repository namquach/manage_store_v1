<?php

namespace App\Http\Livewire\OrderLine;

use App\Models\Order;
use App\Models\OrderLine;
use App\Models\Product;
use App\Models\SettingPriceCustomer;
use App\Traits\DispatchBrowserEventTrait;
use Livewire\Component;

class OrderLineForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Product $product */
    public $product;

    /** @var Order $order */
    public $order;

    public $sku;
    public $name;
    public $quantity = 0;
    public $price = 0;
    public $product_id;
    public $unit;
    public $unit_by_can = 0;
    public $more_info = [];
    public $productUnit = [];
    public $note;
    public $note2;

    public $listeners = [
        'onChangeSku',
        //'emitUpdateOrderFromOrderLineForm'
    ];

    public function rules()
    {
        return [
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required|gt:0',
            'quantity' => 'required|numeric|gt:0',
            'product_id' => '',
            'unit' => 'required',
        ];
    }

    public function render()
    {
        if(!empty($this->sku))
        {
            $this->more_info = [
                'unit_by_can' => $this->unit_by_can,
                'exchange_by_can' => $this->unit == 1 ? (int)$this->quantity : qhn_format_number((int)$this->quantity / $this->unit_by_can),
                'quantity' => $this->unit == 1 ? (int)$this->quantity * $this->unit_by_can : (int)$this->quantity,
            ];
        }

        return view('livewire.order-line.order-line-form');
    }

    public function create()
    {
        $this->validate();

        $orderLine = new OrderLine();
        $orderLine->product_id = $this->product_id;
        $orderLine->order_id = $this->order->id;
        $orderLine->price = $this->price;
        $orderLine->quantity = $this->unit != 1 ? (int)$this->quantity : (int)$this->quantity * $this->unit_by_can;
        $orderLine->note = $this->note;
        $orderLine->note2 = $this->note2;
        $orderLine->unit = $this->unit;
        $orderLine->unit_by_can = $this->unit_by_can;
        $orderLine->save();

        $this->reset([
            'name',
            'sku',
            'price',
            'quantity',
            'note'
        ]);

        $this->dispatchBrowserEvent('event.autofocusSelect2Sku');
        $this->dispatchBrowserAlertify('success', 'Add product success');
        $this->emit('reloadComponent');
    }

    //public function emitUpdateOrderFromOrderLineForm($order){
    //    $this->order->customer_id = $order['customer_id'];
    //}

    public function onChangeSku($sku)
    {
        $product = Product::findOne('sku', $sku);

        if($product !== null)
        {
            $price = 0;

            if(!empty($this->order->customer_id)){
                $settingPriceCustomer = SettingPriceCustomer::query()
                                        ->join('setting_price_products', 'setting_price_products.setting_price_id', '=', 'setting_price_customers.setting_price_id')
                                        ->join('setting_prices', 'setting_prices.id', '=', 'setting_price_products.setting_price_id')
                                        ->where('setting_price_customers.customer_id', $this->order->customer_id)
                                        ->where('setting_price_products.product_id', $product->id)
                                        ->where('setting_prices.tmp', '')
                                        ->first();
                if(!empty($settingPriceCustomer)){
                    $price = $settingPriceCustomer->price;
                }
            }

            if($price == 0) $price = calculatePrice($product->price, $product->vat);


            dd($product->productUnit->toArray());
            $this->sku = $sku;
            $this->name = $product->name;
            $this->price = $price;
            $this->product_id = $product->id;
            $this->unit = $product->productUnit->id;
            $this->unit_by_can = $product->quantity_container_by_can;
            $this->quantity = 0;
            $this->productUnit = [
                $product->productUnit->toArray(),
                ['id' => 1, 'name' => 'Thùng'],
            ];
        }
    }
}
