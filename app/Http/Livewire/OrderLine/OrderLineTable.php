<?php

namespace App\Http\Livewire\OrderLine;

use App\Components\Component;
use App\Models\Order;
use App\Models\OrderLine;
use App\Traits\FilterTable;
use App\Traits\DispatchBrowserEventTrait;

class OrderLineTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    /**@var Order $order */
    public $order;

    public $stateModalProduct = false;

    public $total = 0;

    public $listeners = ['callbackReceiveProduct', 'delete', 'reloadComponent' => '$refresh'];

    public function render()
    {
        $this->total = OrderLine::sumTotalPriceByOrder($this->order->id);

        return view('livewire.order-line.order-line-table', [
            'orderLines' => OrderLine::search($this->order->id, $this->search)->paginate($this->perPage),
        ]);
    }

    public function eventOpenModalProduct()
    {
        $this->stateModalProduct = true;
        $this->dispatchBrowserEvent('event.stateModelProductChange');
    }

    public function getQueryString()
    {
        return [];
    }

    public function callbackReceiveProduct($product)
    {
        if($this->order->status === qhn_status_completed){
            $this->dispatchBrowserAlertify('error', 'Order have been completed');
        }

        $orderLine = new OrderLine();
        $orderLine->product_id = $product['id'];
        $orderLine->order_id = $this->order->id;

        $orderLine->quantity = $product['quantity'];
        $orderLine->price = calculatePrice((float)$product['price'], $product['vat']);
        $orderLine->save();

        $this->dispatchAlertify('success', 'Add product success');
    }

    public function delete($id){
        if($this->order->status === qhn_status_completed){
            $this->dispatchBrowserAlertify('warning', 'Order completed, can not delete product');
            return false;
        }

        OrderLine::query()->where('id', $id)->delete();
        $this->dispatchBrowserAlertify('success', 'Delete product success');

    }

    public function reloadComponent() { }
}
