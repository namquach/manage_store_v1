<?php

namespace App\Http\Livewire\Supplier;

use App\Components\Component;
use App\Models\Supplier;
use App\Traits\FilterTable;

class ModalSupplierList extends Component
{
    use FilterTable;

    public function render()
    {
        return view('livewire.supplier.modal-supplier-list', [
            'suppliers' => Supplier::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }

    public function getQueryString()
    {
        return [];
    }
}
