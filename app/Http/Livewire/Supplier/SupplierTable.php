<?php

namespace App\Http\Livewire\Supplier;

use App\Components\Component;
use App\Models\Purchase;
use App\Models\Supplier;
use App\Traits\FilterTable;
use App\Traits\DispatchBrowserEventTrait;
use Illuminate\Support\Facades\Auth;

class SupplierTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    protected $listeners = ['delete'];

    public function render()
    {
        return view('livewire.supplier.supplier-table', [
            'suppliers' => Supplier::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }

    public function delete($id)
    {
        $supplierPurchase = Purchase::query()->where('user_id', Auth::user()->id)->where('supplier_id', $id)->first();

        if(!empty($supplierPurchase)){
            $this->dispatchBrowserAlertify('error', 'Supplier have used, can not delete');
            return false;
        }

        Supplier::query()->firstWhere('id' , $id)->delete();
        $this->dispatchAlertify();
    }
}
