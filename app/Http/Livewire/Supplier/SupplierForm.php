<?php

namespace App\Http\Livewire\Supplier;

use App\Components\Component;
use App\Models\District;
use App\Models\Province;
use App\Models\Supplier;
use App\Models\Ward;
use App\Traits\DispatchBrowserEventTrait;

class SupplierForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Supplier $supplier */
    public $supplier;

    public $provinces = [];
    public $districts = [];
    public $wards = [];

    public function rules()
    {
        return [
            'supplier.province' => '',
            'supplier.district' => '',
            'supplier.ward' => '',
            'supplier.name' => 'required|max:50',
            'supplier.address' => 'required|max:150',
            'supplier.note' => 'max:300',
        ];
    }

    public function render()
    {
        $this->provinces = Province::getProvinces();

        if(empty($this->districts)){
            if(!empty($this->supplier->province)){
                $province = Province::query()->firstWhere('name', $this->supplier->province);
                $this->districts = District::getByProvince($province->code);
            }else{
                $this->districts = District::getByProvince($this->provinces->first()->code);
            }
        }

        if(empty($this->wards)){
            if(!empty($this->supplier->district)){
                $province = Province::query()->firstWhere('name', $this->supplier->province);
                $district = District::query()->firstWhere([
                    'name' => $this->supplier->district,
                    'parent_code' => $province->code,
                ]);
                $this->wards = Ward::getByDistrict($district->code);
            }else{
                $this->wards = Ward::getByDistrict($this->districts->first()->code);
            }
        }

        if(empty($this->supplier->province)) $this->supplier->province = $this->provinces->first()->name;
        if(empty($this->supplier->district)) $this->supplier->district = $this->districts->first()->name;
        if(empty($this->supplier->ward)) $this->supplier->ward = $this->wards->first()->name;

        return view('livewire.supplier.supplier-form');
    }

    public function onChangeProvince($event)
    {
        $province = Province::query()->firstWhere('name',  $event);
        $this->districts = District::getByProvince($province->code);
        $this->wards = Ward::getByDistrict($this->districts->first()->code);
    }

    public function onChangeDistrict($event)
    {
        $province = Province::query()->firstWhere('name',  $this->supplier->province);

        $district = District::query()->firstWhere([
            'name' => $event,
            'parent_code' => $province->code,
        ]);

        $this->wards = Ward::getByDistrict($district->code);
    }

    public function submit()
    {
        $this->validate();

        if(Supplier::checkExist($this->supplier)){
            $this->dispatchBrowserAlertify('error', 'Supplier already exist');
            return false;
        }

        $this->supplier->save();

        return redirect()->route('supplier.index');
    }
}
