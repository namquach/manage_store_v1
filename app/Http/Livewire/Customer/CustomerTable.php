<?php

namespace App\Http\Livewire\Customer;

use App\Components\Component;
use App\Models\Customer;
use App\Models\Order;
use App\Models\SettingPriceCustomer;
use App\Traits\FilterTable;
use App\Traits\DispatchBrowserEventTrait;

class CustomerTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    protected $listeners = ['delete'];

    public function render()
    {
        return view('livewire.customer.customer-table', [
            'customers' => Customer::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                                ->paginate($this->perPage),
        ]);
    }

    public function delete($id)
    {
        $customerOrder = Order::query()->where('customer_id', $id)->first();
        $customerSettingPrice = SettingPriceCustomer::query()->firstWhere('customer_id', $id);
        if(!empty($customerOrder) || !$this->emit($customerSettingPrice)){
            $this->dispatchBrowserAlertify('error', 'Customer have use, can not delete');
            return false;
        }

        Customer::query()->firstWhere('id' , $id)->delete();
        $this->dispatchAlertify();
    }
}
