<?php

namespace App\Http\Livewire\Customer;

use App\Components\Component;
use App\Models\Customer;
use App\Traits\FilterTable;

class ModalCustomerList extends Component
{
    use FilterTable;

    public function render()
    {
        return view('livewire.customer.modal-customer-list', [
            'customers' => Customer::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }

    public function getQueryString()
    {
        return [];
    }
}
