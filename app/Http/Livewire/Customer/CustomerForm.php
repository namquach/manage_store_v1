<?php

namespace App\Http\Livewire\Customer;

use App\Components\Component;
use App\Models\Customer;
use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use App\Traits\DispatchBrowserEventTrait;

class CustomerForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Customer $customer */
    public $customer;

    public $provinces = [];
    public $districts = [];
    public $wards = [];

    public function rules()
    {
        return [
            'customer.province' => '',
            'customer.district' => '',
            'customer.ward' => '',
            'customer.name' => 'required|max:50',
            'customer.address' => 'required|max:150',
            'customer.note' => 'max:300',
        ];
    }

    public function render()
    {
        $this->provinces = Province::getProvinces();

        if(empty($this->districts)){
            if(!empty($this->customer->province)){
                $province = Province::query()->firstWhere('name', $this->customer->province);
                $this->districts = District::getByProvince($province->code);
            }else{
                $this->districts = District::getByProvince($this->provinces->first()->code);
            }
        }

        if(empty($this->wards)){
            if(!empty($this->customer->district)){
                $province = Province::query()->firstWhere('name', $this->customer->province);
                $district = District::query()->firstWhere([
                    'name' => $this->customer->district,
                    'parent_code' => $province->code,
                ]);
                $this->wards = Ward::getByDistrict($district->code);
            }else{
                $this->wards = Ward::getByDistrict($this->districts->first()->code);
            }
        }

        if(empty($this->customer->province)) $this->customer->province = $this->provinces->first()->name;
        if(empty($this->customer->district)) $this->customer->district = $this->districts->first()->name;
        if(empty($this->customer->ward)) $this->customer->ward = $this->wards->first()->name;

        return view('livewire.customer.customer-form');
    }

    public function onChangeProvince($event)
    {
        $province = Province::query()->firstWhere('name',  $event);
        $this->districts = District::getByProvince($province->code);
        $this->wards = Ward::getByDistrict($this->districts->first()->code);
    }

    public function onChangeDistrict($event)
    {
        $province = Province::query()->firstWhere('name',  $this->customer->province);

        $district = District::query()->firstWhere([
            'name' => $event,
            'parent_code' => $province->code,
        ]);

        $this->wards = Ward::getByDistrict($district->code);
    }

    public function submit()
    {
        $this->validate();

        if(Customer::checkExist($this->customer)){
            $this->dispatchBrowserAlertify('error', 'Customer already exist');
            return false;
        }

        $this->customer->save();

        return redirect()->route('customer.index');
    }
}
