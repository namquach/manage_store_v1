<?php

namespace App\Http\Livewire\Order;

use App\Components\Component;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderLine;
use App\Traits\DispatchBrowserEventTrait;

class OrderForm extends Component
{
    use DispatchBrowserEventTrait;

    /** @var Order $order */
    public $order;

    public $isCreate;

    public $stateModalCustomer = false;

    public $listeners = ['eventChooseCustomer', 'hideModal'];

    public function rules()
    {
        return [
            'order.customer_id' => 'required',
            'order.customer_name' => 'required|max:50',
            'order.customer_address' => 'required|max:250',
            'order.date' => 'required',
            'order.note' => '',
        ];
    }

    public function render()
    {
        return view('livewire.order.order-form');
    }

    public function eventOpenModalCustomer()
    {
        $this->stateModalCustomer = true;
        $this->dispatchBrowserEvent('event.stateModelCustomerChange');
    }

    public function hideModal() {
        $this->stateModalCustomer = false;
    }

    public function eventChooseCustomer($customer)
    {
        $this->order->customer_id = $customer['id'];
        $this->order->customer_name = $customer['name'];
        $this->order->customer_address = Customer::fullAddress($customer);
        $this->order->save();
        $this->dispatchBrowserEvent('event.closeModal', ['modal' => 'stateModalCustomer']);
        //$this->emit('refreshOrderForm');
        //$this->emit('emitUpdateOrderFromOrderLineForm', $this->order->toArray());
        //dd($this->order->toArray());
        //session()->flash('order' . $this->order->id, $this->order->toArray());
    }

    public function submit()
    {
        $this->validate();

        $checkOrderLine = OrderLine::query()->where('order_id', $this->order->id)->get();

        if($checkOrderLine->count() === 0){
            $this->dispatchBrowserEvent('alertify.confirm.error', [
                'message' => __('Products can not be null'),
            ]);
            return false;
        }

        $this->order->tmp = '';
        $this->order->save();

        $this->dispatchBrowserEvent('alertify.confirm.success', [
            'message' => __('Create order success'),
        ]);

        return redirect()->route('order.index');
    }

    public function updateStatusOrder()
    {
        if($this->order->status !== qhn_status_draft){
            $this->dispatchBrowserAlertify('warning', 'Order completed, can not update status');
            return false;
        }

        $orderLines = OrderLine::query()->where('order_id', $this->order->id)->get();

        if($orderLines->count() === 0){
            $this->dispatchBrowserAlertify('warning', 'Order must have product, can not update status');
            return false;
        }

        $this->order->status = qhn_status_completed;
        $this->order->save();

        foreach ($orderLines as $orderLine)
        {
            $product = $orderLine->product;
            $product->stock = $product->stock - $orderLine->quantity;
            $product->store_out = $product->store_out + $orderLine->quantity;
            $product->save();
        }

        $this->dispatchBrowserAlertify('success', 'Completed status order success');
        $this->emit('reloadComponent');
    }
}
