<?php

namespace App\Http\Livewire\Order;

use App\Components\Component;
use App\Models\Order;
use App\Traits\DispatchBrowserEventTrait;
use App\Traits\FilterTable;

class OrderTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    public $stateModal = false;

    public $listeners = ['delete'];

    public $status;

    public function render()
    {
        $orderProvider = Order::search($this->search)->orderBy($this->orderBy, $this->orderAsc ? 'asc' : 'desc');

        if(!empty($this->status)) $orderProvider->where('status', $this->status);

        return view('livewire.order.order-table', [
            'orders' => $orderProvider->paginate($this->perPage)
        ]);
    }

    public function delete($id)
    {
        $order = Order::query()->firstOrFail([
            'id' => $id,
            'user_id' => \Auth::user()->id,
        ]);

        if($order->status === qhn_status_completed){
            $this->dispatchBrowserAlertify('warning', 'Order completed, can not delete');
            return false;
        }

        $order->delete();
        $this->dispatchBrowserAlertify('success', 'Delete order success');
    }
}
