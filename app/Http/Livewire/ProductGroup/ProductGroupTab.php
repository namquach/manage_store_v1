<?php

namespace App\Http\Livewire\ProductGroup;

use Livewire\Component;

class ProductGroupTab extends Component
{
    public $tabList = [
        1 => 'Group 1',
        2 =>'Group 2',
        3 => 'Group 3',
    ];

    public $tabIndex = 1;

    public function render()
    {
        return view('livewire.product-group.product-group-tab', [
            'tabIndex' => $this->tabIndex,
        ]);
    }

    public function eventOnNextTab($tabIndex)
    {
        $this->tabIndex = $tabIndex;
        $this->emit('changeGroup', $tabIndex);
    }
}
