<?php

namespace App\Http\Livewire\ProductGroup;

use App\Components\Component;
use App\Models\ProductGroup;
use Illuminate\Support\Facades\Auth;

class ProductGroupForm extends Component
{
    /** @var ProductGroup $productGroup */
    public $productGroup;

    public $name;

    public $group = [
        ['key' => 1, 'value' => 'Group 1'],
        ['key' => 2, 'value'=> 'Group 2'],
        ['key' => 3, 'value' => 'Group 3'],
    ];

    public function render()
    {
        return view('livewire.product-group.product-group-form');
    }

    public function  rules()
    {
        return [
            'productGroup.name' => "required|string|max:50|unique:product_groups,name," . $this->productGroup->id,
            'productGroup.group' => 'required',
        ];
    }

    public function submit()
    {
        $this->validate();
        $this->productGroup->user_id = Auth::id();
        $this->productGroup->save();

        return redirect()->route('product-group.index');
    }
}
