<?php

namespace App\Http\Livewire\ProductGroup;

use App\Components\Component;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Traits\DispatchBrowserEventTrait;
use App\Traits\FilterTable;

class ProductGroupTable extends Component
{
    use FilterTable, DispatchBrowserEventTrait;

    public $group;

    protected $listeners = [
        'delete', 'changeGroup'
    ];

    public function render()
    {
        return view('livewire.product-group.product-group-table', [
            'productGroups' => ProductGroup::search($this->search, $this->group)->orderBy( $this->orderBy, $this->orderAsc ? 'asc' : 'desc')
                ->paginate($this->perPage)
        ]);
    }

    public function delete($id) {
        $check = Product::query()->firstWhere('group_1', $id);

        if($check){
            $this->dispatchBrowserAlertify('warning', 'Product group used, can not delete');
            return false;
        }

        ProductGroup::query()->firstWhere('id', $id)->delete();

        $this->dispatchAlertify();
    }

    public function changeGroup($group){
        $this->group = $group;
    }
}
