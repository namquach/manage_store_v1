<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class ProductUnit
 * @package App\Models
 * @property int $id
 * @property string $name
 */
class ProductUnit extends Model
{
    protected $table = 'product_units';

    protected $fillable = [
        'name'
    ];

    public static function getAll()
    {
        return self::query()->whereNotIn('id', [1])->get();
    }
}
