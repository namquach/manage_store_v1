<?php

namespace App\Models;


use App\Components\Model;

/***
 * Class Ward
 * @package App\Models
 * @property int $id
 * @property string $parent_code
 * @property string $name
 * @property string $prefix
 */
class Ward extends Model
{
    protected $table = 'wards';
    public $timestamps = false;

    protected $fillable = [
        'prefix',
        'name',
        'parent_code'
    ];

    public static function getByDistrict($districtParentCode)
    {
        return self::query()->where('parent_code', $districtParentCode)->orderBy('name')->get();
    }
}
