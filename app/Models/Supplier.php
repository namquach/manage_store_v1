<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Support\Facades\Auth;

/***
 * Class Customer
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $province
 * @property string $district
 * @property string $ward
 * @property string $address
 * @property string $note
 * @property string $crated_at
 * @property string $updated_at
 * @property integer $user_id
 */
class Supplier extends Model
{
    protected $table = 'suppliers';

    protected $fillable = [
        'name',
        'province',
        'district',
        'ward',
        'address',
        'note',
        'user_id',
    ];

    public static function search(string $search)
    {
        $query = self::query()->where('user_id', \Auth::user()->id);

        return empty($search) ? $query : $query->where('name', 'like', '%' . $search . '%')
            ->orWhere('province', 'like', '%' . $search . '%')
            ->orWhere('district', 'like', '%' . $search . '%')
            ->orWhere('ward', 'like', '%' . $search . '%')
            ->orWhere('address', 'like', '%' . $search . '%');
    }

    public static function fullAddress($customer)
    {
        if(is_array($customer)) $customer = (object)$customer;

        return join(', ', [
            $customer->address,
            $customer->ward,
            $customer->district,
            $customer->province,
        ]);
    }

    public static function checkExist(Supplier $customer)
    {
        $check = self::query()->where([
            'name' => $customer->name,
            'address' => $customer->address,
            'ward' => $customer->ward,
            'district' => $customer->district,
            'province' => $customer->province,
            'user_id' => Auth::user()->id,
        ])->first();

        return !empty($check);
    }
}


