<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class ProductGroup
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property integer $user_id
 * @property int $group
 */
class ProductGroup extends Model
{
    protected $table = 'product_groups';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'user_id',
        'group'
    ];

    public static function search($search, $group)
    {
        $query = self::query()->where([
            'user_id' => \Auth::user()->id,
            'group' => $group
        ]);
        return empty($search) ? $query : $query->where('name', 'like', '%' . $search . '%');
    }

    public function rules()
    {
        return [
            'product-group.name' => 'required|max:50|unique:product_groups,name,' . $this->id,
        ];
    }

    public static function getDataRenderSelect() : array
    {
        $arrProductGroup = [];
        self::query()->where('user_id', \Auth::user()->id)->get()->map(function ($pg) use(&$arrProductGroup) {
            $arrProductGroup[$pg->id] = $pg->name;
        });
        return $arrProductGroup;
    }
}
