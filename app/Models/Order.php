<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/***
 * Class Order
 * @package App\Models
 * @property int $id
 * @property string $date
 * @property int $customer_id
 * @property string $customer_name
 * @property string $customer_address
 * @property string $note
 * @property string $payment
 * @property string $tmp
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 */
class Order extends Model
{
    const STATUS_DRAFT = 'draft';
    const STATUS_COMPLETED = 'completed';

    protected $table = 'orders';

    protected $fillable = [
        'date',
        'customer_id',
        'customer_name',
        'customer_address',
        'note',
        'payment',
        'tmp',
        'status',
        'user_id'
    ];

    public static function search(string $search)
    {
        $query = self::query()->where('tmp', "")->where('user_id', \Auth::user()->id);
        return empty($search)
            ? $query
            : $query->where(function ($query) use ($search) {
                    $query->orWhere('date', 'like', '%' . $search . '%')
                        ->orWhere('status', 'like', '%' . $search . '%')
                        ->orWhere('customer_name', 'like', '%' . $search . '%')
                        ->orWhere('customer_address', 'like', '%' . $search . '%');
                });
    }
}
