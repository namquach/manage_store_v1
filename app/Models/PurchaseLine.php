<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/***
 * Class PurchaseLine
 * @package App\Models
 * @property int $id
 * @property int $purchase_id
 * @property int $product_id
 * @property int $quantity
 * @property float $price
 * @property string $created_at
 * @property string $updated_at
 * @property int $unit
 * @property int $unit_by_can
 * @property string $note
 * @property string $note2
 */
class PurchaseLine extends Model
{
    protected $table = 'purchase_lines';

    protected $fillable = [
        'purchase_id',
        'product_id',
        'quantity',
        'price',
        'unit_by_can',
        'unit',
        'note',
        'note2',
    ];

    public static function search($purchase_id, $search)
    {
        return self::query()->with(['product'])->where('purchase_id', $purchase_id)->whereHas('product', function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        });
    }

    public static function findOneByCondition($product_id, $purchase_id)
    {
        return self::query()->where([
            'product_id' => $product_id,
            'purchase_id' => $purchase_id
        ])->first();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public static function sumTotalPriceByPurchase($purchase_id)
    {
        return self::query()->where('purchase_id', $purchase_id)->sum(DB::raw('price * quantity'));
    }
}
