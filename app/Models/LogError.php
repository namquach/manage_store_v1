<?php

namespace App\Models;


use App\Components\ModelMongo;

class LogError extends ModelMongo
{
    protected $collection = 'log_error';

    protected $primaryKey = '_id';

    protected $fillable = [
        'message',
        'line',
        'code',
        'file',
        'trace',
        'previous',
        'trace_as_string',
        'user_id',
        'created_at',
    ];

    public static function search(string $search)
    {
        $query = self::query();

        return empty($search) ? $query : $query->where('message', 'like', '%' . $search . '%');
    }
}
