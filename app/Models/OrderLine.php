<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/***
 * Class OrderLine
 * @package App\Models
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $quantity
 * @property float $price
 * @property string $code
 * @property int $unit
 * @property int $unit_by_can
 * @property string $note
 * @property string $note2
 */
class OrderLine extends Model
{
    use HasFactory;

    protected $table = 'order_lines';

    protected $fillable = [
        'order_id',
        'price',
        'quantity',
        'price',
        'code',
        'unit',
        'unit_by_can',
        'note',
        'note2',
    ];

    public static function search($order_id, string $search)
    {
        $query = self::query()->with(['product'])->where('order_id', $order_id);

        return empty($search) ? $query : $query->whereHas('product', function ($productQuery ) use ($search) {
            $productQuery->where('name', 'like', '%' . $search . '%') ;
        });
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public static function findOneByCondition($product_id, $order_id)
    {
        return self::query()->where([
            'product_id' => $product_id,
            'order_id' => $order_id
        ])->first();
    }

    public static function sumTotalPriceByOrder($order_id)
    {
        return self::query()->where('order_id', $order_id)->sum(DB::raw('price * quantity'));
    }

}
