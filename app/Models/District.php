<?php

namespace App\Models;


use App\Components\Model;

/***
 * Class District
 * @package App\Models
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $parent_code
 * @property string $prefix
 */
class District extends Model
{
    protected $table = 'districts';
    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
        'parent_code',
        'number'
    ];

    public static function getByProvince($provinceParentCode)
    {
        return self::query()->where('parent_code', $provinceParentCode)->orderBy('name')->get();
    }
}
