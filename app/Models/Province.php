<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class Province
 * @package App\Models
 * @property int $id
 * @property string $code
 * @property string $name
 */
class Province extends Model
{
    protected $table = 'provinces';
    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
    ];

    public static function getProvinces()
    {
        return self::query()->orderBy('name')->get();
    }
}
