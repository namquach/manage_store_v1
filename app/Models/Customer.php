<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/***
 * Class Customer
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $province
 * @property string $district
 * @property string $ward
 * @property string $address
 * @property string $note
 * @property string $crated_at
 * @property string $updated_at
 * @property integer $user_id
 */
class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'name',
        'province',
        'district',
        'ward',
        'address',
        'note',
        'user_id',
    ];

    public static function search(string $search)
    {
        $query = self::query()->where('user_id', \Auth::user()->id);

        return empty($search) ? $query : $query->where('name', 'like', '%' . $search . '%')
                                                        ->orWhere('province', 'like', '%' . $search . '%')
                                                        ->orWhere('district', 'like', '%' . $search . '%')
                                                        ->orWhere('ward', 'like', '%' . $search . '%')
                                                        ->orWhere('address', 'like', '%' . $search . '%');
    }

    public static function searchSelect2($search) {
        $query = self::query()->where('user_id', \Auth::user()->id);
        return empty($search) ? $query : $query->where('name', 'like', '%' .$search . '%');
    }

    public static function fullAddress($customer)
    {
        if(is_array($customer)) $customer = (object)$customer;

        return join(', ', [
            $customer->address,
            $customer->ward,
            $customer->district,
            $customer->province,
        ]);
    }

    public static function checkExist(Customer $customer)
    {
        $check = Customer::query()->where([
            'name' => $customer->name,
            'address' => $customer->address,
            'ward' => $customer->ward,
            'district' => $customer->district,
            'province' => $customer->province,
            'user_id' => \Auth::user()->id,
        ])->first();

        return !empty($check);
    }
}




