<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use phpDocumentor\Reflection\Types\Self_;

/***
 * Class Purchase
 * @package App\Models
 * @property int $id
 * @property string $date
 * @property string $note
 * @property string $status
 * @property int $supplier_id
 * @property string $supplier_name
 * @property string $supplier_address
 * @property string $tmp
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 */
class Purchase extends Model
{
    const STATUS_DRAFT = 'draft';
    const STATUS_COMPLETED = 'completed';

    protected $table = 'purchases';

    protected $fillable = [
        'date',
        'note',
        'status',
        'supplier_id',
        'supplier_name',
        'supplier_address',
        'tmp',
        'user_id',
    ];

    public static function search(string $search)
    {
        $query = self::query()->where('tmp', '')->where('user_id', \Auth::user()->id);
        return empty($search)
            ? $query
            : $query->where(function ($query) use ($search) {
                            $query->orWhere('date', 'like', '%' . $search . '%')
                                ->orWhere(' status', 'like', '%' . $search . '%')
                                ->orWhere('supplier_name', 'like', '%' . $search . '%')
                                ->orWhere('supplier_address', 'like', '%' . $search . '%');
                        });
    }
}
