<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class SettingPriceCustomer
 * @package App\Models
 * @property int $setting_price_id
 * @property int $customer_id
 */
class SettingPriceCustomer extends Model
{
    protected $table = 'setting_price_customers';

    protected $primaryKey = ['customer_id', 'setting_price_id'];

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'setting_price_id',
        'customer_id',
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public static function getBySettingPrice($setting_price_id)
    {
        return self::query()->with(['customer'])->where('setting_price_id', $setting_price_id)->get();
    }
}
