<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class ProductCustomer
 * @package App\Models
 * @property int $customer_id
 * @property int $product_id
 * @property float $price1
 * @property float $price2
 */
class ProductCustomer extends Model
{
    protected $table = 'product_customers';
    protected $primaryKey = ['customer_id', 'product_id'];

    public $timestamps = false;

    protected $fillable = [
        'customer_id',
        'product_id',
        'price1',
        'price2',
    ];

    public function customers()
    {

    }

    public function products()
    {

    }
}
