<?php

namespace App\Models;

use App\Components\Model;

/****
 * Class SettingPriceProduct
 * @package App\Models
 * @property int $id
 * @property int $setting_price_id
 * @property int $product_id
 * @property float $price
 */
class SettingPriceProduct extends Model
{
    protected $table = 'setting_price_products';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'setting_price_id',
        'product_id',
        'price',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public static function search($setting_price_id, string $search)
    {
        $query = self::query()->with(['product'])->where('setting_price_id', $setting_price_id);

        return empty($search) ?$query : $query->whereHas('product', function ($query) use($search) {
                                                    $query->where('name', 'like', '%' . $search . '%');
                                                });
    }
}
