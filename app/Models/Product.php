<?php

namespace App\Models;

use App\Components\Model;
use Illuminate\Support\Facades\Auth;

/***
 * Class Product
 * @package App\Models
 * @property int $id
 * @property int $group1
 * @property int $group2
 * @property int $group3
 * @property string $sku
 * @property string $name
 * @property float $price
 * @property float $vat
 * @property float $quantity_container_by_can
 * @property string $note
 * @property int $stock
 * @property int $store_in
 * @property int $store_out
 * @property string $created_at
 * @property string $updated_at
 * @property int $unit
 * @property integer $user_id
 */
class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'group1',
        'group2',
        'group3',
        'sku',
        'name',
        'price',
        'vat',
        'quantity_container_by_can',
        'note',
        'stock',
        'store_in',
        'store_out',
        'unit',
    ];

    public static function search($search)
    {
        $query = self::query()->with(['productGroup'])->where('user_id', Auth::user()->id);
        return empty($search) ? $query
            : $query->where('sku', 'like', '%' . $search . '%')
                                                ->orWhere('name', 'like', '%' . $search . '%');
    }

    public static function searchAutocomplete($search)
    {
        $query = self::query()->with(['productGroup'])->where('user_id', Auth::user()->id);
        return empty($search) ? $query
            : $query->where('sku', 'like', '%' . $search . '%')
                ->orWhere('name', 'like', '%' . $search . '%');
    }

    public static function _find($id)
    {
        return self::query()->findOrFail($id);
    }

    public static function findOne($attribute, $value){
        return self::query()->where($attribute, $value)->where('user_id', Auth::user()->id)->first();
    }

    public function productGroup(){
        return $this->belongsTo(ProductGroup::class, 'group1', 'id');
    }

    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'unit', 'id');
    }
}
