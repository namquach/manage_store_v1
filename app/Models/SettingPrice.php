<?php

namespace App\Models;

use App\Components\Model;

/***
 * Class SettingPrice
 * @package App\Models
 * @property int $id
 * @property integer $user_id
 * @property string $name
 * @property string $tmp
 * @property string $created_at
 * @property string $updated_at
 */
class SettingPrice extends Model
{
    protected $table = 'setting_prices';

    protected $fillable = [
        'name',
        'tmp',
        'user_id',
    ];

    public static function search(string $search)
    {
        $query = self::query()->where('user_id', \Auth::user()->id)->where('tmp', '');
        return empty($search) ? $query : $query->where('name', 'like', '%' . $search . '%');
    }
}
