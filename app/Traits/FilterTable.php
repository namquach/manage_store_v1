<?php

namespace App\Traits;

trait FilterTable
{
    public $search = '';
    public $perPage = 10;
    public $orderBy = 'id';
    public $orderAsc = true;

    public $selectAll = false;
    public $checkAllData = [];

    public $filterPerPages = [
        10 => 10,
        20 => 20,
        50 => 50,
        100 => 100
    ];

    public function deleteConfirm($id) : void
    {
        $this->dispatchBrowserEvent('swal.confirm', [
            'type' => 'warning',
            'title' => 'Are you sure ?',
            'id' => $id,
            'text' => '',
            'icon' => 'warning',
        ]);
    }

    public function dispatchAlertify(string $type = 'success', string $message = 'Delete item success!')
    {
        $this->dispatchBrowserEvent('alertify.confirm.' . $type, [
            'message' => trans('messages.' . $message),
        ]);
    }
}
