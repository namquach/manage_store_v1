<?php
namespace App\Traits;

trait DispatchBrowserEventTrait
{
    public function dispatchBrowserAlertify(string $type, string $message)
    {
        $this->dispatchBrowserEvent('alertify.confirm.' . $type, [
            'message' => __($message),
        ]);
    }
}
