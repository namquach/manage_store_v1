<?php
namespace App\Components;

class ModelMongo extends \Jenssegers\Mongodb\Eloquent\Model
{
    protected $connection = 'mongodb';
}
