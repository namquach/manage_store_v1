<?php
namespace App\Components;

use Livewire\WithPagination;

class Component extends \Livewire\Component
{
    use WithPagination;

    public function paginationView()
    {
        return 'vendor.livewire.custom';
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

//    public function getQueryString()
//    {
//        return [];
//    }
}
